#!/bin/gnuplot

# png image set
#set terminal pngcairo  transparent enhanced font "Times, 40" fontscale 1.0 size 1000, 800 

# pdf image set
#set terminal pdfcairo  transparent enhanced font "Times-New-Roman, 40" fontscale 1.0 size 2000, 1800 

# real time terminal
set term qt enhanced font "Times-New-Roman, 10" size 800,600

# read in argument
num_output = ARG1

# set output filename
#outName = sprintf("heatmap-%s.png", num_output)
#set output outName

# Various ways to create a 2D heat map from ascii data

#set title "Heat Map generated from a file containing Z values only"
#set title outName
unset key
set tic scale 0

# Color runs from white to green
set palette rgbformula 21,22,23

# set heat range
#set cbrange [0:5]

set cblabel "Score"
unset cbtics

#set xrange [-0.5:4.5]
#set yrange [-0.5:4.5]

set view map
num_output = ARG1

#datName = sprintf("gridE.%s", num_output)
#splot datName matrix with image

do for[i=1:num_output] {
	datName = sprintf("gridE.%i", i)
	set title datName
	splot datName matrix with image
	pause 0.01
}

pause -1 "Press ENTER to exit\n"

