#!/bin/python3

import sys
import numpy as np
import numba as nb
from numba import int32

@nb.njit(cache=True)
def calNewWidth(boxInfo, xGridWidth, yGridWidth):
    xAxisIndex = 0
    yAxisIndex = 1
    lowIndex = 0
    highIndex = 1

    nx = np.floor( (boxInfo[xAxisIndex, highIndex]-boxInfo[xAxisIndex, lowIndex])/xGridWidth )
    ny = np.floor( (boxInfo[yAxisIndex, highIndex]-boxInfo[yAxisIndex, lowIndex])/yGridWidth )

    xGridWidth_new = (boxInfo[xAxisIndex, highIndex]-boxInfo[xAxisIndex, lowIndex])/nx
    yGridWidth_new = (boxInfo[yAxisIndex, highIndex]-boxInfo[yAxisIndex, lowIndex])/ny

    nx = int(nx)
    ny = int(ny)

    return nx, ny, xGridWidth_new, yGridWidth_new;

