#!/bin/gnuplot

# read user inputs
alloy = ARG1
seed = ARG2
dataName1 = sprintf("src/%s.txt", ARG3)
dataName2 = sprintf("src/%s.txt", ARG4)

# png image set
set terminal pngcairo background "#FFFFFF" enhanced font "Times-New-Roman, 20" fontscale 1.0 size 1400, 1000

# pdf image set
#set terminal pdfcairo  transparent enhanced font "Times-New-Roman, 40" fontscale 1.0 size 2000, 1800 
#set terminal pdfcairo enhanced font "Times-New-Roman, 40" fontscale 1.0 size 2000, 1800 

# set output filename
outName = sprintf("figures/GSFE_AVGcorrelation_%s_%s.png", alloy, seed)
set output outName

# multiplot
titleName = sprintf("Average Correlation %s seed %s", alloy, seed)
set multiplot title titleName layout 2,1 margins 0.1,0.9,0.1,0.95 spacing 0.07

# enable grid
set grid linetype 1 linecolor "grey"

# top plot
set xtics 1
set ytics 0.2
plot dataName1 u 1 with lines lc rgb "black" linewidth 3 title sprintf("x[112] correlation")

# bottom plot
#set format x "%g"
set ytics 0.2
plot dataName2 u 1 with lines lc rgb "black" linewidth 3 title sprintf("y[110] correlation") 
