#!/bin/python3

import numpy as np
import numba as nb

@nb.njit(cache=True, parallel=True)
# average the data for each unique dx (or dy) value
def AVGdata(CorDat: np.ndarray) -> np.ndarray:
    # extract the max number of dx (or dy) step value
    firstRow = 0
    stepNumLimit = len(CorDat[firstRow])

    # Calculate the average per each step
    CorAVG = np.zeros(stepNumLimit)
    for i in range(stepNumLimit):
        CorAVG[i] += np.mean(CorDat[:, i])

    return CorAVG;
