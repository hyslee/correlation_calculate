#!/bin/gnuplot

# read user inputs
alloy = ARG1
seed = ARG2
dataName = sprintf("src/%s.txt", ARG3)

# png image set
set terminal pngcairo background "#FFFFFF" enhanced font "Times-New-Roman, 20" fontscale 1.0 size 1400, 1000
#set terminal pngcairo background "#000000" transparent enhanced font "Times-New-Roman, 20" fontscale 1.0 size 1000, 1000

# pdf image set
#set terminal pdfcairo  transparent enhanced font "Times-New-Roman, 40" fontscale 1.0 size 2000, 1800 
#set terminal pdfcairo enhanced font "Times-New-Roman, 40" fontscale 1.0 size 2000, 1800 

# set output filename
outName = sprintf("figures/GSFE_correlation_%s_%s_x.png", alloy, seed)
set output outName

# multiplot
titleName = sprintf("x[112] correlation %s seed %s", alloy, seed)
set multiplot title titleName layout 2,1 margins 0.1,0.9,0.1,0.95 spacing 0.05

# enable grid
set grid linetype 1 linecolor "grey"

# Measure the length of column 1
#stats dataName using 1 nooutput
#length = STATS_records

# Output the length
#print "Length of column 1: ", length

#set xrange [0:9]
#set palette rgbformulae 33,13,10
#set palette defined (0 'blue', 0.5 'white', 1 'red')
#plot for [i=1:length] dataName u i with lines linewidth 3 title sprintf("row %i",i)

# top plot
set xtics 1 format ""
set ytics 0.2
plot for [i=1:10] dataName u i with lines linewidth 3 title sprintf("row %i",i)

# bottom plot
set format x "%g"
set ytics 0.2
plot for [i=11:20] dataName u i with lines linewidth 3 title sprintf("row %i",i)
