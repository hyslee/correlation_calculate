#!/bin/python3

import numpy as np
import numba as nb
import sys, os

@nb.njit(cache=True, parallel=True)
def calAutoCor(GFE, normalize: int=0):
    firstRowIndex = 0
    firstColumnIndex = 0
    # count the number of columns on the first row
    xGridNumber = len(GFE[firstRowIndex])
    # count the number of rows on the first column
    yGridNumber = len(GFE[:,firstColumnIndex])

    # set the limit on the measuring correlation distance 
    #dx_limit = int(xGridNumber/2)
    #dy_limit = int(yGridNumber/2)
    dx_limit = int(xGridNumber)
    dy_limit = int(yGridNumber)

    # initialize the correlaition distance(modified to show periodicity)
    #dxx = np.arange(1, dx_limit+1)
    #dyy = np.arange(1, dy_limit+1)
    dxx = np.arange(0, dx_limit+1)
    dyy = np.arange(0, dy_limit+1)

    # single row test in x direction
    NxIndex = xGridNumber # index starts from 0
    NyIndex = yGridNumber

    # Measure the autocorrelation in x direction
    xMultircoeff = np.zeros((yGridNumber, len(dxx)))
    for y in range(NyIndex):
        #rcoeff = np.zeros(len(dxx))
        for dx in dxx:
            rc = 0 # accumulator
            for x in range(NxIndex):
                if normalize:
                    xRowMean = GFE[y].mean()
                    xRowSTD = GFE[y].std()
                    #print(f'GFE[y] = {GFE[y]}')
                    #print(f'xRowMean = {xRowMean}')
                    #print(f'xRowSTD = {xRowSTD}')
                # sets the ref point based on x,y and iterate over x with dx
                x1 = x
                # periodic condition
                x2 = x+dx if x+dx < NxIndex else x+dx-NxIndex
                if normalize:
                    r = ((GFE[y, x1]-xRowMean)/xRowSTD) * ((GFE[y, x2]-xRowMean)/xRowSTD)
                else:
                    r = GFE[y, x1]*GFE[y, x2]
                rc += r
            #indexCorrected_dx = dx-1
            xMultircoeff[y, dx] =+ rc/NxIndex

    # Measure the autocorrelation in y direction
    yMultircoeff = np.zeros((xGridNumber, len(dyy)))
    for x in range(NxIndex):
        for dy in dyy:
            rc = 0
            for y in range(NyIndex):
                if normalize:
                    yColumnMean = GFE[:, x].mean()
                    yColumnSTD = GFE[:, x].std()
                    #print(f'GFE[:,x] = {GFE[:,x]}')
                    #print(f'yColumnMean = {yColumnMean}')
                    #print(f'yColumnSTD ={yColumnSTD}')
                y1 = y
                # periodic condition
                y2 = y+dy if y+dy < NyIndex else y+dy-NyIndex
                if normalize:
                    r = ((GFE[y1, x]-yColumnMean)/yColumnSTD) * ((GFE[y2, x]-yColumnMean)/yColumnSTD)
                else:
                    r = GFE[y1, x]*GFE[y2, x]
                # random var product
                #r = GFE[y1, x]*GFE[y2, x]
                # sum product
                rc += r
            #indexCorrected_dy = dy-1
            yMultircoeff[x, dy] =+ rc/NyIndex

    return xMultircoeff, yMultircoeff;
    #return xMultircoeff;

