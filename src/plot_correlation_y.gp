#!/bin/gnuplot

# read user inputs
alloy = ARG1
seed = ARG2
dataName = sprintf("src/%s.txt", ARG3)

# png image set
set terminal pngcairo background "#FFFFFF" enhanced font "Times-New-Roman, 20" fontscale 1.0 size 1400, 1000
#set terminal pngcairo background "#000000" transparent enhanced font "Times-New-Roman, 20" fontscale 1.0 size 1000, 1000

# pdf image set
#set terminal pdfcairo  transparent enhanced font "Times-New-Roman, 40" fontscale 1.0 size 2000, 1800 
#set terminal pdfcairo enhanced font "Times-New-Roman, 40" fontscale 1.0 size 2000, 1800 

# set output filename
outName = sprintf("figures/GSFE_correlation_%s_%s_y.png", alloy, seed)
set output outName

titleName = sprintf("y[110] correlation %s seed %s", alloy, seed)
set title titleName

# enable grid
set grid linetype 1 linecolor "grey"

# Measure the length of column 1
stats dataName using 1 nooutput
length = STATS_records

# Output the length
#print "Length of column 1: ", length

#set xrange [0:9]
#set palette rgbformulae 33,13,10
set palette defined (0 'blue', 0.5 'white', 1 'red')
#plot for [i=1:length] dataName u i with lines linewidth 3 title sprintf("row %i",i)
#plot for [i=1:5] dataName u i with lines linewidth 3 title sprintf("row %i",i)
plot for [i=1:10] dataName u i with lines linewidth 3 title sprintf("column %i",i)
