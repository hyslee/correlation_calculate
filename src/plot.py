
#!/bin/python3

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import numba as nb

@nb.njit(cache=True, parallel=True)
def generateTicks(nx, ny):
    x = np.arange(0, nx+1, 1)
    y = np.arange(0, ny+1, 1)
    return x, y

def plot(nx, ny, Z, figureName):
    # add LaTeX math syntax support and font setup
    font = {'family': 'serif', 'weight': 'normal', 'size': 13}
    mathfont = {'fontset': 'stix'}
    plt.rc('font', **font)
    plt.rc('mathtext', **mathfont)

    # Create figure object
    fig, ax = plt.figure(figsize=(8,6), dpi=200), plt.subplot(1,1,1)

    # set color palette
    colors = sns.color_palette("rocket", as_cmap=True)

    # set ticks
    #x = np.arange(0, nx+1, 1)
    #y = np.arange(0, ny+1, 1)
    x, y = generateTicks(nx, ny)

    # draw
    #pc = ax.pcolormesh(X, Y, Z, cmap=colors)
    pc = ax.pcolormesh(Z.T, cmap=colors, rasterized=True)

    # add colorbar on the plot
    fig.colorbar(pc, ax=ax, label=r'$Energy$')

    # set proeprteis
    # Show all ticks and label them with the respective list entries
    ax.set_title('')
    ax.set_xlabel(r'x [112]')
    ax.set_ylabel(r'y [110]')
    #ax.set_xlim(1, dist.max() + 1)
    ax.grid(False)
    #ax.legend(loc='upper right', fontsize='small')
    ax.set_xticks(x)
    ax.set_yticks(y)
    #ax.set_xticklabels(x)
    #ax.set_yticklabels(y)


    # save the figure
    fig.savefig(figureName)

