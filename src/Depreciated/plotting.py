#!/bin/python3

import matplotlib.pyplot as plt
import numpy as np

def drawPlot(xRange, yRange, autoCorr):
    # add LaTeX math syntax support and font setup
    font = {'family': 'serif', 'weight': 'normal', 'size': 13}
    mathfont = {'fontset': 'stix'}
    plt.rc('font', **font)
    plt.rc('mathtext', **mathfont)

    # Create figure object
    fig, ax = plt.figure(figsize=(8,6), dpi=200), plt.subplot(1,1,1)
    #fig, ax = plt.subplots()

    # Add plot in the figure
    im = ax.imshow(autoCorr, cmap='viridis')
    ax.set_title(r'AlMg5 GSFE Correlation Function')
    fig.colorbar(im)

    # adjust ticks
    xRangeLabel = [f'{x:1.1f}' for x in xRange]
    yRangeLabel = [f'{y:1.1f}' for y in yRange]
    ax.set_xticks(np.arange(len(xRange)), labels=xRangeLabel)
    ax.set_yticks(np.arange(len(yRange)), labels=yRangeLabel)

    # save the figure
    fig.savefig('./figures/AlMg5.pdf')
