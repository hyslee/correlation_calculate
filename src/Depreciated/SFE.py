#!/bin/python3

import numpy as np
import re, sys, os
from collections import defaultdict
from dataclasses import dataclass
from numba import jit

@dataclass
class stackingFault:
    alloy : str
    E_0 : np.ndarray
    outputPath : str
    #xGridWidth : float
    #yGridWidth : float
    debug : int = 0

    # Global variables
    ## array indexes for atom data
    idIndex = 0
    xAxisIndex = 2
    yAxisIndex = 3
    PEIndex = 5
    KEIndex = 6
    alats = {'AlMg5': 4.053, 'AlMg10': 4.076, 'AlMg15': 4.0988}
    #debugFolder = self.outputPath+'/debugFiles/'

    def calGenE(self, E_f: np.ndarray, xGridWidth: float, yGridWidth: float):
        debugFolder = self.outputPath+'/debugFiles/'

        # mesh the grid using perfect crystal
        atomIDsInGrid, gridCoord, gridSize_SI = self.mesh(xGridWidth, yGridWidth)

        # from perfect crystal (E_0), pull potential energy of each atom based on the atomID
        E_0Bin = defaultdict(list)
        for binID, atomIDs in atomIDsInGrid.items():
            for atom in self.E_0:
                if atom[self.idIndex] in atomIDs:
                    E_0Bin[binID].append(atom[self.PEIndex])

        # Debug information
        if self.debug:
            if not os.access(debugFolder, os.F_OK):
                os.mkdir(debugFolder)
            # for debugging energy
            f = open(debugFolder+'atomE_bin.txt', 'w')
            for k, v in E_0Bin.items():
                f.write(str(k)+' : '+str(v)+'\n')
            f.close()

        # sum energy of each grid (E_0)
        sumE_0Bin = defaultdict(list)
        for binID, atomE in E_0Bin.items():
            sumE_0Bin[binID].append(np.sum(atomE))

        # Debug information
        if self.debug:
            # for debugging sum energy
            f = open(debugFolder+'atomSumE_bin.txt', 'w')
            for k, v in sumE_0Bin.items():
                f.write(str(k)+' : '+str(v)+'\n')
            f.close()

        # pull faulted potential energy of each atom based on the saved atom ID
        E_fBin = defaultdict(list)
        for binID, atomIDs in atomIDsInGrid.items():
            for atom in E_f:
                if atom[self.idIndex] in atomIDs:
                    E_fBin[binID].append(atom[self.PEIndex])

        # for debugging energy
        if self.debug:
            f = open(debugFolder+'atomISFE_bin.txt', 'w')
            for k, v in E_fBin.items():
                f.write(str(k)+' : '+str(v)+'\n')
            f.close()

        # sum faulted energy in each grid
        sumE_fBin = defaultdict(list)
        for binID, atomE in E_fBin.items():
            sumE_fBin[binID].append(np.sum(atomE))

        # for debugging sum energy
        if self.debug:
            f = open(debugFolder+'atomISFSumE_bin.txt', 'w')
            for k, v in sumE_fBin.items():
                f.write(str(k)+' : '+str(v)+'\n')
            f.close()

        # Conversion Constants
        eV_to_mJ = 1.602176633e-16
        # Calculate GSFE 
        GFEBin = defaultdict(list)
        for binID, FE in sumE_fBin.items():
            FE = np.array(FE)
            E0 = np.array(sumE_0Bin[binID])
            GFE = FE - E0
            # Convert the unit from eV to mJ/m^2
            GFE = (GFE*eV_to_mJ) / gridSize_SI
            GFE = GFE.item()
            GFEBin[binID].append(GFE)

        # for debugging GSFE
        if self.debug:
            f = open(debugFolder+'GFEBin.txt', 'w')
            for k, v in GFEBin.items():
                f.write(str(k)+' : '+str(v)+'\n')
            f.close()

        # Make the coordinates of the grid for the energy as the center of the grid
        gridTotalData = []
        for binID, Energy in GFEBin.items():
            gCoords = gridCoord[binID][0]
            gCoords_X = gCoords[0]
            gCoords_Y = gCoords[1]
            GFE = Energy[0]
            gridTotalData.append([gCoords_X, gCoords_Y, GFE])
        gridTotalData = np.array(gridTotalData)

        # Debug
        if self.debug:
            f = open(debugFolder+'gridTotalData.txt', 'w')
            for i in range(len(gridTotalData)):
                f.write(str(gridTotalData[i])+'\n')
            f.close()

        # Make the GSFE as a mesh grid for correlation calculation
        xGridNum, yGridNum = len(np.unique(gridTotalData[:,0])) ,len(np.unique(gridTotalData[:,1]))
        rowNum = yGridNum
        columnNum = xGridNum
        GFEmesh = np.array(gridTotalData[:,2]).reshape(rowNum, columnNum)

        # Debug mesh 
        if self.debug:
            f = open(debugFolder+'meshData.txt', 'w')
            for i in range(len(GFEmesh)):
                string = np.array2string(GFEmesh[i], max_line_width=200)
                f.write(string)
                f.write('\n')
            f.close()

        return GFEmesh;

    # mesh the grid based on the grid size, 
    # each grid ID contains atom IDs from dumpFiles
    def mesh(self, xGridWidth: float, yGridWidth: float) -> dict:
        debugFolder = self.outputPath+'/debugFiles/'

        # find the lowest atom coordinate
        xlow = self.E_0[:, self.xAxisIndex].min()
        ylow = self.E_0[:, self.yAxisIndex].min()

        # find the highest atom coordinate
        xhigh = self.E_0[:, self.xAxisIndex].max()
        yhigh = self.E_0[:, self.yAxisIndex].max()
        #print(f'xlow = {xlow} ylow = {ylow} xhigh = {xhigh} yhigh = {yhigh}')

        # Read lattice parameter of the alloy
        latParameter = self.alats[self.alloy]

        # set grid size in x and y direction
        xlatdist = xGridWidth
        ylatdist = yGridWidth
        #xlatdist = latParameter*np.sqrt((-1)**2 + 1**2 + 2**2)/2
        #ylatdist = latParameter*np.sqrt(1**2 + 1**2 + 0**2)/2
        #print(f'xlatdist = {xlatdist} ylatdist = {ylatdist}')

        gridAtomBin = defaultdict(list)
        gridCoord = defaultdict(list)
        gridData = []

        # Set grid start and end points
        xStart = xlow+xlatdist
        xEnd = xhigh
        yStart = ylow+ylatdist
        yEnd = yhigh

        # set Area of each grid in meter
        gridSize_SI = xlatdist*1e-10 * ylatdist*1e-10

        # Make grid
        xRange = np.arange(xStart, xEnd+xlatdist, xlatdist)
        yRange = np.arange(yStart, yEnd+ylatdist, ylatdist)
        print(f'xRange = {xRange} yRange = {yRange}')
        exit()

        # bin atoms in each grid
        binID = 0
        for x in xRange:
            for y in yRange:
                for atom in self.E_0:
                    if x == xStart and y == yStart:
                        if atom[self.xAxisIndex] < x and atom[self.yAxisIndex] < y:
                            gridAtomBin[binID].append(atom[self.idIndex])
                    elif x == xEnd and y == yStart:
                        if atom[self.xAxisIndex] > x and atom[self.yAxisIndex] < y:
                            gridAtomBin[binID].append(atom[self.idIndex])
                    elif x == xStart and y == yEnd:
                        if atom[self.xAxisIndex] < x and atom[self.yAxisIndex] > y:
                            gridAtomBin[binID].append(atom[self.idIndex])
                    elif x == xEnd and y == yEnd:
                        if atom[self.xAxisIndex] > x and atom[self.yAxisIndex] > y:
                            gridAtomBin[binID].append(atom[self.idIndex])
                    elif x == xStart:
                        if atom[self.xAxisIndex] < x and y-ylatdist < atom[self.yAxisIndex] < y:
                            gridAtomBin[binID].append(atom[self.idIndex]) 
                    elif x == xEnd:
                        if atom[self.xAxisIndex] > x and y-ylatdist < atom[self.yAxisIndex] < y:
                            gridAtomBin[binID].append(atom[self.idIndex])
                    elif y == yStart:
                        if x-xlatdist < atom[self.xAxisIndex] < x and atom[self.yAxisIndex] < y:
                            gridAtomBin[binID].append(atom[self.idIndex])
                    elif y == yEnd:
                        if x-xlatdist < atom[self.xAxisIndex] < x and atom[self.yAxisIndex] > y:
                            gridAtomBin[binID].append(atom[self.idIndex])
                    else:
                        if x-xlatdist < atom[self.xAxisIndex] < x and y-ylatdist < atom[self.yAxisIndex] < y:
                            gridAtomBin[binID].append(atom[self.idIndex])
                CoordX, CoordY = x, y
                gridCoord[binID].append([x, y])
                binID += 1

        if self.debug:
            if not os.access(debugFolder, os.F_OK):
                os.mkdir(debugFolder)
            ## Write binned atom data for debugging
            # number of atoms in each grid
            #f = open(debugFolder+'atomNum_bin.txt', 'w')
            #sums = 0
            #for k, v in gridAtomBin.items():
            #    f.write(str(k)+' : '+str(len(v))+'\n')
            #    sums += len(v)
            #f.write(f'totalSum : {sums}')
            #f.close()
            with open(f'{debugFolder}atomNum_bin.txt', 'w') as f:
                f.write(f'#gridID #NumberofAtoms\n')
                sums = 0
                for k, v in gridAtomBin.items():
                    #f.write(str(k)+' : '+str(len(v))+'\n')
                    f.write(f'{k} {len(v)}\n')
                    sums += len(v)
                f.write(f'totalSum : {sums}')

            # atom IDs in each grid 
            f = open(debugFolder+'atomIDs_bin.txt', 'w')
            sums = 0
            for k, v in gridAtomBin.items():
                # convert atom number to integers
                v = [int(x) for x in v]
                f.write(str(k)+' : '+str(v)+'\n')
                sums += len(v)
            f.write(f'totalSum : {sums}')
            f.close()
            # coordinates of each grid
            f = open(debugFolder+'Coord_bin.txt', 'w')
            for k, v in gridCoord.items():
                f.write(str(k)+' : '+str(v)+'\n')
            f.close()

        return gridAtomBin, gridCoord, gridSize_SI;


