#!/bin/python3

import sys, os
import numpy as np
from collections import defaultdict

def mesh_and_E_grid(atomList, debug=0):
    if debug:
        debugFolder = './debugFiles/'
        if not os.access(debugFolder, os.F_OK):
            os.mkdir(debugFolder)

    # array indexes for atom data
    idIndex = 0
    xAxisIndex = 2
    yAxisIndex = 3
    PEIndex = 5
    KEIndex = 6

    # find the lowest atom coordinate
    xlow = atomList[:, xAxisIndex].min()
    ylow = atomList[:, yAxisIndex].min()

    # find the highest atom coordinate
    xhigh = atomList[:, xAxisIndex].max()
    yhigh = atomList[:, yAxisIndex].max()
    print(f'xlow = {xlow} ylow = {ylow} xhigh = {xhigh} yhigh = {yhigh}')

    # Read box boundary coordinates
    #idIndex = 0
    latParameter = 4.053
    boxLabelLine = 5
    xlatdist = latParameter*np.sqrt((-1)**2 + 1**2 + 2**2)/2
    ylatdist = latParameter*np.sqrt(1**2 + 1**2 + 0**2)/2
    print(f'xlatdist = {xlatdist} ylatdist = {ylatdist}')

    gridAtomBin = defaultdict(list)
    gridCoord = defaultdict(list)
    gridData = []

    # Set grid start and end points
    xStart = xlow+xlatdist
    xEnd = xhigh
    yStart = ylow+ylatdist
    yEnd = yhigh

    # set Area of each grid
    gridA = xlatdist*1e-10 * ylatdist*1e-10

    # Make grid
    xRange = np.arange(xStart, xEnd+xlatdist, xlatdist)
    yRange = np.arange(yStart, yEnd+ylatdist, ylatdist)
    print(f'xRange = {xRange} yRange = {yRange}')

    # bin atoms in each grid
    binID = 0
    for x in xRange:
        for y in yRange:
            for atom in atomList:
                if x == xStart and y == yStart:
                    if atom[xAxisIndex] < x and atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif x == xEnd and y == yStart:
                    if atom[xAxisIndex] > x and atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif x == xStart and y == yEnd:
                    if atom[xAxisIndex] < x and atom[yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif x == xEnd and y == yEnd:
                    if atom[xAxisIndex] > x and atom[yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif x == xStart:
                    if atom[xAxisIndex] < x and y-ylatdist < atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex]) 
                elif x == xEnd:
                    if atom[xAxisIndex] > x and y-ylatdist < atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif y == yStart:
                    if x-xlatdist < atom[xAxisIndex] < x and atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif y == yEnd:
                    if x-xlatdist < atom[xAxisIndex] < x and atom[yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[idIndex])
                else:
                    if x-xlatdist < atom[xAxisIndex] < x and y-ylatdist < atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
            CoordX, CoordY = x, y
            gridCoord[binID].append([x, y])
            binID += 1

    if debug:
        ## Write binned atom data for debugging
        # number of atoms in each grid
        f = open(debugFolder+'atomNum_bin.txt', 'w')
        sums = 0
        for k, v in gridAtomBin.items():
            f.write(str(k)+' : '+str(len(v))+'\n')
            sums += len(v)
        f.write(f'totalSum : {sums}')
        f.close()
        # atom IDs in each grid 
        f = open(debugFolder+'atomIDs_bin.txt', 'w')
        sums = 0
        for k, v in gridAtomBin.items():
            # convert atom number to integers
            v = [int(x) for x in v]
            f.write(str(k)+' : '+str(v)+'\n')
            sums += len(v)
        f.write(f'totalSum : {sums}')
        f.close()
        # coordinates of each grid
        f = open(debugFolder+'Coord_bin.txt', 'w')
        for k, v in gridCoord.items():
            f.write(str(k)+' : '+str(v)+'\n')
        f.close()

    # pull potential energy based on atom ID
    gridEnergyBin = defaultdict(list)
    for binID, atomIDs in gridAtomBin.items():
        for atom in atomList:
            if atom[idIndex] in atomIDs:
                gridEnergyBin[binID].append(atom[PEIndex])

    # Debug information
    if debug:
        # for debugging energy
        f = open(debugFolder+'atomE_bin.txt', 'w')
        for k, v in gridEnergyBin.items():
            f.write(str(k)+' : '+str(v)+'\n')
        f.close()

    # sum energy per grid
    gridSumEBin = defaultdict(list)
    for binID, atomE in gridEnergyBin.items():
        gridSumEBin[binID].append(np.sum(atomE))

    # Debug information
    if debug:
        # for debugging sum energy
        f = open(debugFolder+'atomSumE_bin.txt', 'w')
        for k, v in gridSumEBin.items():
            f.write(str(k)+' : '+str(v)+'\n')
        f.close()
        print(gridSumEBin)

    return gridSumEBin;
