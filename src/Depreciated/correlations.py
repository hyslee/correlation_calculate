import numpy as np
import re, sys, os
from dataclasses import dataclass

@dataclass
class correlation:
    outputPath : str = None
    debug : int = 0

    def calAutoCor(self, GFE, normalize: int=0):
        firstRowIndex = 0
        firstColumnIndex = 0
        # count the number of columns on the first row
        xGridNumber = len(GFE[firstRowIndex,:])
        # count the number of rows on the first column
        yGridNumber = len(GFE[:,firstColumnIndex])

        # set the limit on the measuring correlation distance 
        dx_limit = int(xGridNumber/2)
        dy_limit = int(yGridNumber/2)

        # initialize the correlaition distance
        dxx = np.arange(1, dx_limit+1)
        dyy = np.arange(1, dy_limit+1)

        # single row test in x direction
        rcoeff = []
        NxIndex = xGridNumber # index starts from 0
        NyIndex = yGridNumber

        # Measure the autocorrelation in x direction
        xMultircoeff = []
        for y in range(NyIndex):
            rcoeff = []
            for dx in dxx:
                rc = 0 # accumulator
                for x in range(NxIndex):
                    if normalize:
                        xRowMean = GFE[y].mean()
                        xRowSTD = GFE[y].std()
                        #print(f'GFE[y] = {GFE[y]}')
                        #print(f'xRowMean = {xRowMean}')
                        #print(f'xRowSTD = {xRowSTD}')
                    # sets the ref point based on x,y and iterate over x with dx
                    x1 = x
                    # periodic condition
                    x2 = x+dx if x+dx < NxIndex else x+dx-NxIndex
                    if normalize:
                        r = ((GFE[y, x1]-xRowMean)/xRowSTD) * ((GFE[y, x2]-xRowMean)/xRowSTD)
                    else:
                        r = GFE[y, x1]*GFE[y, x2]
                    rc += r
                rcoeff.append(rc/NxIndex)
            xMultircoeff.append(rcoeff)

        # Measure the autocorrelation in y direction
        yMultircoeff = []
        for x in range(NxIndex):
            rcoeff = []
            for dy in dyy:
                rc = 0
                for y in range(NyIndex):
                    if normalize:
                        yColumnMean = GFE[:, x].mean()
                        yColumnSTD = GFE[:, x].std()
                        #print(f'GFE[:,x] = {GFE[:,x]}')
                        #print(f'yColumnMean = {yColumnMean}')
                        #print(f'yColumnSTD ={yColumnSTD}')
                    y1 = y
                    # periodic condition
                    y2 = y+dy if y+dy < NyIndex else y+dy-NyIndex
                    if normalize:
                        r = ((GFE[y1, x]-yColumnMean)/yColumnSTD) * ((GFE[y2, x]-yColumnMean)/yColumnSTD)
                    else:
                        r = GFE[y1, x]*GFE[y2, x]
                    # random var product
                    #r = GFE[y1, x]*GFE[y2, x]
                    # sum product
                    rc += r
                rcoeff.append(rc/NyIndex)
            yMultircoeff.append(rcoeff)

        if self.debug:
            f = open(self.outputPath+'xCorCoefficient.txt', 'w')
            g = open(self.outputPath+'yCorCoefficient.txt', 'w')
            for xline in xMultircoeff:
                f.write(str(xline)+'\n')
            for yline in yMultircoeff:
                g.write(str(yline)+'\n')
            f.close()
            g.close()

        return xMultircoeff, yMultircoeff

    def calAutoCorAVG(self, GFE):
        firstRowIndex = 0
        firstColumnIndex = 0
        # count the number of columns on the first row
        xGridNumber = len(GFE[firstRowIndex,:])
        # count the number of rows on the first column
        yGridNumber = len(GFE[:,firstColumnIndex])

        # set the limit on the measuring correlation distance 
        dx_limit = int(xGridNumber/2)
        dy_limit = int(yGridNumber/2)

        # initialize the correlaition distance
        dxx = np.arange(1, dx_limit+1)
        dyy = np.arange(1, dy_limit+1)

        # single row test in x direction
        rcoeff = []
        NxIndex = xGridNumber # index starts from 0
        NyIndex = yGridNumber

        print(f'NxIndex = {NxIndex}')
        print(f'NyIndex = {NyIndex}')

        # Measure the autocorrelation in x direction
        xMultircoeff = []
        for y in range(NyIndex):
            rcoeff = []
            for dx in dxx:
                rc = 0 # accumulator
                for x in range(NxIndex):
                    print(f'x, y = {x}, {y}')
                    #if normalize: 
                    #    xRowMean = GFE[x].mean()
                    #    xRowSTD = GFE[x].std()
                    # sets the ref point based on x,y and iterate over x with dx
                    x1 = x
                    # periodic condition
                    x2 = x+dx if x+dx < NxIndex else x+dx-NxIndex
                    print(f'x1 = {x1}, x2= {x2}')
                    r = GFE[y, x1] * GFE[y, x2]
                    #if normalize:
                    #    r = (r - xRowMean)/xRowSTD
                    rc += r
                rcoeff.append(rc/NxIndex)
            xMultircoeff.append(rcoeff)

        # calulcate average in x row
        xMultircoeff = np.matrix(xMultircoeff)
        print(f'xMultircoeff = {xMultircoeff}')
        print(f'{len(xMultircoeff)}')
        xMultircoeffAVG = []
        for i in range(dx_limit):
            sumColumn = xMultircoeff[:,i].sum()
            numRows = len(xMultircoeff[:,i])
            #avg = xMultircoeff[:,i].sum()/len()
            avg = sumColumn / numRows
            print(f'avg = {avg}')
            xMultircoeffAVG.append(avg)
        #print(xMultircoeffAVG)
        #exit()

        # Measure the autocorrelation in y direction
        yMultircoeff = []
        for x in range(NxIndex):
            rcoeff = []
            for dy in dyy:
                rc = 0
                for y in range(NyIndex):
                    y1 = y
                    # periodic condition
                    y2 = y+dy if y+dy < NyIndex else y+dy-NyIndex
                    # random var product
                    r = GFE[y1, x]*GFE[y2, x]
                    # sum product
                    rc += r
                rcoeff.append(rc/NyIndex)
            yMultircoeff.append(rcoeff)

        # calulcate average in y row
        yMultircoeff = np.matrix(yMultircoeff)
        print(f'yMultircoeff = {yMultircoeff}')
        print(f'{len(yMultircoeff)}')
        yMultircoeffAVG = []
        for i in range(dy_limit):
            sumColumn = yMultircoeff[:,i].sum()
            numRows = len(yMultircoeff[:,i])
            #avg = xMultircoeff[:,i].sum()/len()
            avg = sumColumn / numRows
            print(f'avg = {avg}')
            yMultircoeffAVG.append(avg)
        print(yMultircoeffAVG)

        if self.debug:
            f = open(self.outputPath+'xCorCoefficient.txt', 'w')
            g = open(self.outputPath+'yCorCoefficient.txt', 'w')
            for xline in xMultircoeff:
                f.write(str(xline)+'\n')
            for yline in yMultircoeff:
                g.write(str(yline)+'\n')
            f.close()
            g.close()

        return xMultircoeffAVG, yMultircoeffAVG

    # average the data for each unique dx (or dy) value
    def AVGdata(self, CorDat):
        # make numpy matrix out of correlation data
        CorDat_matrix = np.matrix(CorDat)

        # extract the max number of dx (or dy) step value
        firstRow = 0
        stepNumLimit = len(CorDat[firstRow])

        # Calculate the average per each step
        CorAVG = []
        for i in range(stepNumLimit):
            CorAVG.append(CorDat_matrix[:, i].mean())

        return CorAVG;

