#!/bin/python3

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from dataclasses import dataclass

@dataclass
class plots:
    title : str
    xlabel : str
    ylabel : str

    def perRowPlot(self, coeff, figureName):
        # add LaTeX math syntax support and font setup
        font = {'family': 'serif', 'weight': 'normal', 'size': 13}
        mathfont = {'fontset': 'stix'}
        plt.rc('font', **font)
        plt.rc('mathtext', **mathfont)

        # color palette
        colors = sns.color_palette("bright")
        # if there is more data than num of colors
        if len(coeff) > len(colors):
            fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(9, 4.5), tight_layout=True, sharey=True)
            ax = ax1
            i, j = 0, 0
            firstDataIndex = 0
            # tested distance for autocorrelation
            dist = np.arange(1, len(coeff[firstDataIndex])+1)
            while i < len(coeff):
                if i == len(colors):
                    j = 0
                    ax = ax2
                if ax == ax2 and j == len(colors):
                    break;
                labels = 'line '+str(i)
                ax.plot(dist, coeff[i], label=labels, color=colors[j])
                i += 1
                j += 1
            ax1.grid(True)
            ax2.grid(True)
            ax1.legend(loc='upper right', fontsize='xx-small')
            ax2.legend(loc='upper right', fontsize='xx-small')
            ax1.set_xlim(1, dist.max() + 1)
            ax2.set_xlim(1, dist.max() + 1)
            ax1.set_xlabel(self.xlabel)
            ax1.set_ylabel(self.ylabel)
            # save figure
            fig.savefig(figureName)
        else:
            # Create figure object
            fig, ax = plt.figure(figsize=(8,6), dpi=200), plt.subplot(1,1,1)

            # Add plot in the figure
            firstDataIndex = 0
            dist = np.arange(1, len(coeff[firstDataIndex])+1)
            #dist = np.arange(1, len(coeff)+1)

            # draw
            numLines = len(coeff)
            for i in range(numLines):
                labels = 'line'+str(i)
                ax.plot(dist, coeff[i], label=labels, color=colors[i])

            # set proeprteis
            #ax.set_title(self.title)
            ax.set_xlabel(self.xlabel)
            ax.set_ylabel(self.ylabel)
            ax.set_xlim(1, dist.max() + 1)
            ax.grid(True)
            ax.legend(loc='upper right', fontsize='small')

            # save the figure
            fig.savefig(figureName)

    def avgPlot(self, coeff, figureName):
        # add LaTeX math syntax support and font setup
        font = {'family': 'serif', 'weight': 'normal', 'size': 13}
        mathfont = {'fontset': 'stix'}
        plt.rc('font', **font)
        plt.rc('mathtext', **mathfont)

        # Create figure object
        fig, ax = plt.figure(figsize=(8,6), dpi=200), plt.subplot(1,1,1)

        # Add plot in the figure
        firstDataIndex = 0
        dist = np.arange(1, len(coeff)+1)

        # set color palette
        #colors = sns.color_palette("bright")

        # draw
        #numLines = len(coeff)
        ax.plot(dist, coeff, 'k-o', label='average')

        # set proeprteis
        ax.set_title(self.title)
        ax.set_xlabel(self.xlabel)
        ax.set_ylabel(self.ylabel)
        ax.set_xlim(1, dist.max() + 1)
        ax.grid(True)
        ax.legend(loc='upper right', fontsize='small')

        # save the figure
        fig.savefig(figureName)
