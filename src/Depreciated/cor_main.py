#!/bin/python3
import sys
import numpy as np

sys.path.insert(1,'./src') # add library directory path
import mesh as mh
import rawAtomData as rd
import SFE as sf
import correlations as cl
import plots as pl

def main():
    try:
        userInputIndex = 1
        seedInputIndex = 2
        alloy = sys.argv[userInputIndex]
        seed = sys.argv[seedInputIndex]
    except IndexError:
        print('you need to provide an argument,\n1st arg: AlMg5, AlMg10, AlMg15,\n2snd arg: seedNumber')
        sys.exit(1)

    match alloy:
        case 'AlMg5':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg5_v2/AlMg5_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg5_s{seed}/'
        case 'AlMg10':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg10/AlMg10_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg10_s{seed}/'
        case 'AlMg15':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg15/AlMg15_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg15_s{seed}/'
        case _:
            print('Invalid Argument')
            sys.exit(1)

    # Create raw atom data object
    RAWATOM = rd.rawAtomData(alloy, dataPath, debug=0)

    # Call raw atom data for each energy position
    E0raw = RAWATOM.loadDumpData(Epos='0')
    ISFraw = RAWATOM.loadDumpData(Epos='ISF')

    # Grid Parameters
    latParameter = {'AlMg5': 4.053, 'AlMg10': 4.076, 'AlMg15': 4.0988}
    xGridWidth = latParameter[alloy]*np.sqrt((-1)**2 + 1**2 + 2**2)/2
    yGridWidth = latParameter[alloy]*np.sqrt(1**2 + 1**2 + 0**2)/2
    print(f'xGridWidth = {xGridWidth} yGridWidth = {yGridWidth}')

    # calculate grid energy with interpolation
    # 1. read in perfect crystal
    # 2. mesh the grid (make patches)
    # 3. distribute misfit energy of each atom on grid points.(with misfit energy)
    #   - if you create 3x3 patches, you will have 4x4 energy grid
    ### 1. mesh grid
    ### 2. find the row and column index (pos_x/dx and pos_y/dy)
    ###     - index of np.array would be grid index

    ### 3. find the grid points and calculate the area fractions 
    ### 4. calculate the weight factors w1 = A4/At, w2 = A3/At, w3 = A2/At, w4 = A1/At
    ### 5. accumulate energy on each grid points (keep track of energies on grid points)


    # Create stacking fault object
    SF = sf.stackingFault(alloy, E0raw, outputPath, debug=1)

    # Calculate generalized fault energy for intrinsic energy position on mesh grid
    GISFE = SF.calGenE(ISFraw, xGridWidth, yGridWidth)
    #GUSFE = SF.calGenE(USFraw)

    # Create correlation object
    CORR = cl.correlation(outputPath+'debugFiles/', debug=1)

    # Calculate pearson corrleation (make normalize = 0 if you want raw correlation coeff)
    co_x, co_y = CORR.calAutoCor(GISFE, normalize=1)

    # average
    co_x_avg = CORR.AVGdata(co_x)
    co_y_avg = CORR.AVGdata(co_y)

    # plot labels
    title = alloy
    xlabel = 'Step Size (d)'
    ylabel = 'Correlation Coeff'
    figureNameX = outputPath + alloy + 'x.pdf'
    figureNameY = outputPath + alloy + 'y.pdf'

    # Create plotting object
    PL = pl.plots(title, xlabel, ylabel)

    # draw per row correlation plots
    PL.perRowPlot(co_x, figureNameX)
    PL.perRowPlot(co_y, figureNameY)

    # draw average correlation plot
    figureAVGx = outputPath + alloy + 'AVGx.pdf'
    figureAVGy = outputPath + alloy + 'AVGy.pdf'
    PL.avgPlot(co_x_avg, figureAVGx)
    PL.avgPlot(co_y_avg, figureAVGy)

if __name__ == '__main__':
    main()
