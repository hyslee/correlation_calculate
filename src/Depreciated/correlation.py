#!/bin/python3

import sys
import numpy as np
import pandas as pd
#from scipy.signal import correlate2d
#import matplotlib.pyplot as plt
from collections import defaultdict

# add my own libraries
sys.path.insert(1,'./src') # library directory path
import plotting as p
import loadDumpData as ld
import generateEnergyGrid as ge

def main():
    # Presets
    dataPath = '/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg5_v2/AlMg5_seed1/dumpfiles/'
    alloy = 'AlMg5'

    # latttice parameters
    alats = {'AlMg5': 4.053, 'AlMg10': 4.076, 'AlMg15': 4.0988}

    # Load atom data from target energy position
    EnergyPosition = '0' # perfect crystal
    atomList = ld.loadDumpData(alloy, dataPath, EnergyPosition)

    # Mesh the grid and return energy per each grid
    E0grid = ge.mesh_and_E_grid(atomList, debug=1)

    print(E0grid)
    exit()
    # array indexes for atom data
    idIndex = 0
    xAxisIndex = 2
    yAxisIndex = 3
    PEIndex = 5
    KEIndex = 6

    # find the lowest atom coordinate
    xlow = atomList[:, xAxisIndex].min()
    ylow = atomList[:, yAxisIndex].min()

    # find the highest atom coordinate
    xhigh = atomList[:, xAxisIndex].max()
    yhigh = atomList[:, yAxisIndex].max()
    print(f'xlow = {xlow} ylow = {ylow} xhigh = {xhigh} yhigh = {yhigh}')

    # Read box boundary coordinates
    #idIndex = 0
    latParameter = 4.053
    boxLabelLine = 5
    xlatdist = latParameter*np.sqrt((-1)**2 + 1**2 + 2**2)/2
    ylatdist = latParameter*np.sqrt(1**2 + 1**2 + 0**2)/2
    print(f'xlatdist = {xlatdist} ylatdist = {ylatdist}')

    gridAtomBin = defaultdict(list)
    gridCoord = defaultdict(list)
    gridData = []

    # Set grid start and end points
    xStart = xlow+xlatdist
    xEnd = xhigh
    yStart = ylow+ylatdist
    yEnd = yhigh

    # set Area of each grid
    gridA = xlatdist*1e-10 * ylatdist*1e-10

    # Make grid
    xRange = np.arange(xStart, xEnd+xlatdist, xlatdist)
    yRange = np.arange(yStart, yEnd+ylatdist, ylatdist)
    print(f'xRange = {xRange} yRange = {yRange}')

    # binning
    binID = 0
    for x in xRange:
        for y in yRange:
            for atom in atomList:
                if x == xStart and y == yStart:
                    if atom[xAxisIndex] < x and atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif x == xEnd and y == yStart:
                    if atom[xAxisIndex] > x and atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif x == xStart and y == yEnd:
                    if atom[xAxisIndex] < x and atom[yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif x == xEnd and y == yEnd:
                    if atom[xAxisIndex] > x and atom[yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif x == xStart:
                    if atom[xAxisIndex] < x and y-ylatdist < atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex]) 
                elif x == xEnd:
                    if atom[xAxisIndex] > x and y-ylatdist < atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif y == yStart:
                    if x-xlatdist < atom[xAxisIndex] < x and atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
                elif y == yEnd:
                    if x-xlatdist < atom[xAxisIndex] < x and atom[yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[idIndex])
                else:
                    if x-xlatdist < atom[xAxisIndex] < x and y-ylatdist < atom[yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[idIndex])
            CoordX, CoordY = x, y
            gridCoord[binID].append([x, y])
            binID += 1

    ## Write binned atom data for debugging
    # number of atoms in each grid
    f = open('atomNum_bin.txt', 'w')
    sums = 0
    for k, v in gridAtomBin.items():
        f.write(str(k)+' : '+str(len(v))+'\n')
        sums += len(v)
    f.write(f'totalSum : {sums}')
    f.close()
    # atom IDs in each grid 
    f = open('atomIDs_bin.txt', 'w')
    sums = 0
    for k, v in gridAtomBin.items():
        # convert atom number to integers
        v = [int(x) for x in v]
        f.write(str(k)+' : '+str(v)+'\n')
        sums += len(v)
    f.write(f'totalSum : {sums}')
    f.close()
    # coordinates of each grid
    f = open('Coord_bin.txt', 'w')
    for k, v in gridCoord.items():
        f.write(str(k)+' : '+str(v)+'\n')
    f.close()

    # pull potential energy based on atom ID
    gridEnergyBin = defaultdict(list)
    for binID, atomIDs in gridAtomBin.items():
        for atom in atomList:
            if atom[idIndex] in atomIDs:
                gridEnergyBin[binID].append(atom[PEIndex])

    # for debugging energy
    f = open('atomE_bin.txt', 'w')
    for k, v in gridEnergyBin.items():
        f.write(str(k)+' : '+str(v)+'\n')
    f.close()

    # sum energy per grid
    gridSumEBin = defaultdict(list)
    for binID, atomE in gridEnergyBin.items():
        gridSumEBin[binID].append(np.sum(atomE))

    # for debugging sum energy
    f = open('atomSumE_bin.txt', 'w')
    for k, v in gridSumEBin.items():
        f.write(str(k)+' : '+str(v)+'\n')
    f.close()
    print(gridSumEBin)
    
    # Load ISF Sim box info
    dumpFile = 'AlMg5_ISF.20048'

    boxLabelLineStart = 5
    boxInfoISF = np.loadtxt(dataPath+dumpFile, skiprows=boxLabelLineStart, max_rows=3)

    # Load ISF atom data
    dataLabelLine = 9
    atomListISF = np.loadtxt(dataPath+dumpFile, skiprows=dataLabelLine)

    # pull ISF potential energy based on atom ID
    gridISFEnergyBin = defaultdict(list)
    for binID, atomIDs in gridAtomBin.items():
        for atom in atomListISF:
            if atom[idIndex] in atomIDs:
                gridISFEnergyBin[binID].append(atom[PEIndex])

    # for debugging energy
    f = open('atomISFE_bin.txt', 'w')
    for k, v in gridISFEnergyBin.items():
        f.write(str(k)+' : '+str(v)+'\n')
    f.close()

    # sum ISF energy per grid
    gridISFSumEBin = defaultdict(list)
    for binID, atomE in gridISFEnergyBin.items():
        gridISFSumEBin[binID].append(np.sum(atomE))

    # for debugging sum energy
    f = open('atomISFSumE_bin.txt', 'w')
    for k, v in gridISFSumEBin.items():
        f.write(str(k)+' : '+str(v)+'\n')
    f.close()

    # Conversion Constants
    eV_to_mJ = 1.602176633e-16
    # Calculate GSFE 
    gridGSFEBin = defaultdict(list)
    for binID, ISFE in gridISFSumEBin.items():
        ISFE = np.array(ISFE)
        E0 = np.array(gridSumEBin[binID])
        GSFE = ISFE - E0
        # Convert the unit from eV to mJ/m^2
        GSFE = (GSFE*eV_to_mJ) / gridA
        GSFE = GSFE.item()
        gridGSFEBin[binID].append(GSFE)

    # for debugging GSFE
    f = open('GSFEBin.txt', 'w')
    for k, v in gridGSFEBin.items():
        f.write(str(k)+' : '+str(v)+'\n')

    # Make the coordinates of the grid for the energy as the center of the grid
    gridTotalData = []
    for binID, Energy in gridGSFEBin.items():
        gCoords = gridCoord[binID][0]
        gCoords_X = gCoords[0]
        gCoords_Y = gCoords[1]
        GSFE = Energy[0]
        gridTotalData.append([gCoords_X, gCoords_Y, GSFE])
    gridTotalData = np.array(gridTotalData)

    f = open('gridTotalData.txt', 'w')
    for i in range(len(gridTotalData)):
        f.write(str(gridTotalData[i])+'\n')

    # 1-D algo
    # <r(x) r(x+d)> = (r1*r2 + r2*r3 + r3*r4 + r4*r5 + r5*r1)/5
    # <r(x) r(x+2d)> = (r1*r3 + r2*r4 + r3*r5 + r4*r2 + r5*r3)/5
    # <r(x) r(x+3d)> = (r1*r5 + r2r1 + r3r2 + r4r3 + r5r4)/5 -> same as the first one, so exclude
    # 2-D algo
    # r_xy = sum_i^n(xi-meanx)() / sqrt(sum_i^n_i=1(x_i - meanx))*sqrt(sum_i^n_i=1(y_i - meany))
    # in my case the random variable is the same

    # make a 10x10 meshgrid for grid visualization
    x, y = np.meshgrid(np.unique(gridTotalData[:,0]), np.unique(gridTotalData[:,1]))

    # Make the GSFE as a 10x10 mesh grid for correlation calculation
    GSFEmesh = np.array(gridTotalData[:,2]).reshape(10, 10)

    # x correlation, only in x direction (1step at a time when y direction)
    dx_limit = int(len(xRange)/2)
    dy_limit = int(len(yRange)/2)
    dxx = np.arange(1, dx_limit+1)
    dyy = np.arange(1, dy_limit+1)
    #rc = 0 # accumulator

    # single row test in x direction
    rcoeff = []
    Nx = len(xRange) # number of element in x
    Ny = len(yRange) # number of element in y
    for dx in dxx:
        rc = 0
        for x in range(Nx):
            # sets the ref point based on x,y and iterate over x with dx
            x1 = x  
            # periodic condition
            x2 = x + dx if x+dx <= Nx-1 else x + dx - Nx
            #r = GSFEmesh[x1][0] * GSFEmesh[x2][0]
            r = GSFEmesh[0, x1] * GSFEmesh[0, x2]
            rc += r
        rcoeff.append(rc/Nx)
    print(f'(single row) rcoeff = {rcoeff}')
    print(f'avg GSFE = {np.mean(GSFEmesh[0])}')

    # multiple row test in x direction
    # Check if we need to average them or not
    # tiling is another option
    rcoeff = []
    Nx = len(xRange) # number of element in x
    Ny = len(yRange) # number of element in y
    # iterate d with varying delx
    for dx in dxx:
        rc2 = 0 # accumulator2
        # iterate along column (y axis)
        for y in range(Ny):
            rc = 0 # acuumulator1
            # iterate along row (x axis)
            for x in range(Nx):
                # sets the ref point based on x,y and iterate over x with dx
                x1 = x  
                # periodic condition
                x2 = x + dx if x+dx <= Nx-1 else x + dx - Nx
                #r = GSFEmesh[x1][y] * GSFEmesh[x2][y]
                r = GSFEmesh[y, x1] * GSFEmesh[y, x2]
                rc += r
            rc2 += rc/Nx
        rcoeff.append(rc2/Ny)
    print(f'(multiple rows) rcoeff = {rcoeff}')

    # single row test in y direction
    rcoeff = []
    Nx = len(xRange) # number of element in x
    Ny = len(yRange) # number of element in y
    for dy in dyy:
        rc = 0
        for y in range(Ny):
            y1 = y  
            # periodic condition
            y2 = y+dy if y+dy <= Ny-1 else y+dy-Ny
            # random var product
            r = GSFEmesh[y1,0]*GSFEmesh[y2,0]
            # sum product
            rc += r
        rcoeff.append(rc/Ny)
    print(f'(single row y) rcoeff = {rcoeff}')

    # multiple row test in x direction
    # Check if we need to average them or not
    # tiling is another option
    rcoeff = []
    Nx = len(xRange) # number of element in x
    Ny = len(yRange) # number of element in y
    # iterate d with varying delx
    for dy in dyy:
        rc2 = 0 # accumulator2
        # iterate along column (y axis)
        for x in range(Nx):
            rc = 0 # acuumulator1
            # iterate along row (x axis)
            for y in range(Ny):
                # sets the ref point based on x,y and iterate over x with dx
                y1 = y  
                # periodic condition
                y2 = y+dy if y+dy <= Ny-1 else y+dy-Ny
                r = GSFEmesh[y1, x] * GSFEmesh[y2, x]
                rc += r
            rc2 += rc/Ny
        rcoeff.append(rc2/Nx)
    print(f'(multiple rows y) rcoeff = {rcoeff}')


    # y correlation only in y direction (1 step at a time when it goes through x direction)
    #GSFEmean = np.mean(GSFEmesh)
    #autoCorr = correlate2d(GSFEmesh-GSFEmean, GSFEmesh-GSFEmean, mode='same')

    ## normalize auto correlation function
    #autoCorr /= np.max(autoCorr)

    ## draw the plot
    #p.drawPlot(xRange, yRange, autoCorr)

    return 0;

if __name__ == '__main__':
    main()

