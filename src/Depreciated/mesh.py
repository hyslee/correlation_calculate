#!/bin/python3

import numpy as np
import numba as nb

def mesh(xGridWidth: float, yGridWidth: float) -> np.ndarray:
    debugFolder = self.outputPath+'/debugFiles/'

    # find the lowest atom coordinate
    xlow = self.E_0[:, self.xAxisIndex].min()
    ylow = self.E_0[:, self.yAxisIndex].min()

    # find the highest atom coordinate
    xhigh = self.E_0[:, self.xAxisIndex].max()
    yhigh = self.E_0[:, self.yAxisIndex].max()
    #print(f'xlow = {xlow} ylow = {ylow} xhigh = {xhigh} yhigh = {yhigh}')

    # Read lattice parameter of the alloy
    latParameter = self.alats[self.alloy]

    # set grid size in x and y direction
    xlatdist = xGridWidth
    ylatdist = yGridWidth
    #xlatdist = latParameter*np.sqrt((-1)**2 + 1**2 + 2**2)/2
    #ylatdist = latParameter*np.sqrt(1**2 + 1**2 + 0**2)/2
    #print(f'xlatdist = {xlatdist} ylatdist = {ylatdist}')

    gridAtomBin = defaultdict(list)
    gridCoord = defaultdict(list)
    gridData = []

    # Set grid start and end points
    xStart = xlow+xlatdist
    xEnd = xhigh
    yStart = ylow+ylatdist
    yEnd = yhigh

    # set Area of each grid in meter
    gridSize_SI = xlatdist*1e-10 * ylatdist*1e-10

    # Make grid
    xRange = np.arange(xStart, xEnd+xlatdist, xlatdist)
    yRange = np.arange(yStart, yEnd+ylatdist, ylatdist)
    #print(f'xRange = {xRange} yRange = {yRange}')

    # bin atoms in each grid
    binID = 0
    for x in xRange:
        for y in yRange:
            for atom in self.E_0:
                if x == xStart and y == yStart:
                    if atom[self.xAxisIndex] < x and atom[self.yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[self.idIndex])
                elif x == xEnd and y == yStart:
                    if atom[self.xAxisIndex] > x and atom[self.yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[self.idIndex])
                elif x == xStart and y == yEnd:
                    if atom[self.xAxisIndex] < x and atom[self.yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[self.idIndex])
                elif x == xEnd and y == yEnd:
                    if atom[self.xAxisIndex] > x and atom[self.yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[self.idIndex])
                elif x == xStart:
                    if atom[self.xAxisIndex] < x and y-ylatdist < atom[self.yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[self.idIndex]) 
                elif x == xEnd:
                    if atom[self.xAxisIndex] > x and y-ylatdist < atom[self.yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[self.idIndex])
                elif y == yStart:
                    if x-xlatdist < atom[self.xAxisIndex] < x and atom[self.yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[self.idIndex])
                elif y == yEnd:
                    if x-xlatdist < atom[self.xAxisIndex] < x and atom[self.yAxisIndex] > y:
                        gridAtomBin[binID].append(atom[self.idIndex])
                else:
                    if x-xlatdist < atom[self.xAxisIndex] < x and y-ylatdist < atom[self.yAxisIndex] < y:
                        gridAtomBin[binID].append(atom[self.idIndex])
            CoordX, CoordY = x, y
            gridCoord[binID].append([x, y])
            binID += 1

    if self.debug:
        if not os.access(debugFolder, os.F_OK):
            os.mkdir(debugFolder)
        ## Write binned atom data for debugging
        # number of atoms in each grid
        #f = open(debugFolder+'atomNum_bin.txt', 'w')
        #sums = 0
        #for k, v in gridAtomBin.items():
        #    f.write(str(k)+' : '+str(len(v))+'\n')
        #    sums += len(v)
        #f.write(f'totalSum : {sums}')
        #f.close()
        with open(f'{debugFolder}atomNum_bin.txt', 'w') as f:
            f.write(f'#gridID #NumberofAtoms\n')
            sums = 0
            for k, v in gridAtomBin.items():
                #f.write(str(k)+' : '+str(len(v))+'\n')
                f.write(f'{k} {len(v)}\n')
                sums += len(v)
            f.write(f'totalSum : {sums}')

        # atom IDs in each grid 
        f = open(debugFolder+'atomIDs_bin.txt', 'w')
        sums = 0
        for k, v in gridAtomBin.items():
            # convert atom number to integers
            v = [int(x) for x in v]
            f.write(str(k)+' : '+str(v)+'\n')
            sums += len(v)
        f.write(f'totalSum : {sums}')
        f.close()
        # coordinates of each grid
        f = open(debugFolder+'Coord_bin.txt', 'w')
        for k, v in gridCoord.items():
            f.write(str(k)+' : '+str(v)+'\n')
        f.close()

        return gridAtomBin, gridCoord, gridSize_SI;

