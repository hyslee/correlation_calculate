#!/bin/python3

import sys, os, re
import numpy as np

def scanDirs(path):
    dirList = []
    with os.scandir(path) as it:
        for entry in it:
            if not entry.name.startswith('.') and not entry.is_dir():
                dirList.append(entry.name)
    return dirList;

# takes dump path and energy position (USF, ISF..) as arguments
def loadDumpData(alloy, dataPath, Epos, debug=0):
    # scan filenames in "dataPath" directory
    fileNames = scanDirs(dataPath)

    # extract Energy position files
    filesAtEpos = []
    for name in fileNames:
        # Find the word that comes right after '_' string
        parse = re.search(r'(?<='+alloy+'_)'+Epos, name)
        if parse:
            filesAtEpos.append(name)

    # find the dumpfile that has the max timestep (equilibrium)
    equilTime = np.array( [ int(x.split('.')[1]) for x in filesAtEpos ] ).max()

    # dump file name that will be loaded (ex) AlMg5_0.5)
    dumpFile = alloy+'_'+Epos+'.'+str(equilTime)

    # Load Sim box info
    #boxLabelLineStart = 5
    #boxInfo = np.loadtxt(dataPath+dumpFile, skiprows=boxLabelLineStart, max_rows=3)

    # Load atom data
    dataLabelLine = 9
    atomList = np.loadtxt(dataPath+dumpFile, skiprows=dataLabelLine)

    # Return atom data
    return atomList;

if __name__ == '__main__':
    # For function testing, if you are using this as a library, this part will not be executed
    dataPath = '/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg5_v2/AlMg5_seed1/dumpfiles/'
    alloy = 'AlMg5'

    re = loadDumpData(alloy, dataPath, '0')
    print(re)
