#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import correlate2d

# Generate random 2D data array
data = np.random.rand(100, 100)
print(data)
exit()

# Calculate autocorrelation function
autocorr = correlate2d(data - np.mean(data), data - np.mean(data), mode='same')

# Normalize autocorrelation function
autocorr /= np.max(autocorr)

# Plot autocorrelation function
fig, ax = plt.subplots()
im = ax.imshow(autocorr, cmap='viridis')
ax.set_title('2D Autocorrelation Function')
fig.colorbar(im)
plt.show()
