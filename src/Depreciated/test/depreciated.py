#!/bin/python3

r_coeff = []
numer_sum=0
denom_x_sum=0
denom_y_sum=0
xiter = 0
# go through each grid point
for i in range(len(xRange)):
    for j in range(len(yRange)):
        # at each grid point, calculate component
        x = i
        y = j 
        dx = d*i
        dy = d*j
        numer_sum += (GSFEmesh[dx_id, dy_id]-meanE)*(GSFEmesh[j]-meanE)
        denom_x_sum += (GSFEmesh[xiter]-meanE)**2
        denom_y_sum += (GSFEmesh[xiter]-meanE)**2
    xiter = i+j

print(numer_sum)
print(denom_x_sum)
print(denom_y_sum)
r_i = numer_sum / (np.sqrt(denom_x_sum) * np.sqrt(denom_y_sum))
# d range: delx <= d <= (n/2) delx
# periodicity: x+d -n*delx
