# Suppose you have a 2D array named 'xyz_values' containing x, y, and z values in each row
xyz_values = [[1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6]]

# Define the x and y range for the matrix
x_range = range(1, 5)
y_range = range(2, 6)

# Create a 2D matrix of z values based on x and y values
z_matrix = [[0 for y in y_range] for x in x_range]

# Iterate over the x and y values, and populate the matrix with z values
for row in xyz_values:
    x, y, z = row
    if x in x_range and y in y_range:
        z_matrix[x-1][y-2] = z

# Output the 2D matrix of z values
print(z_matrix)
