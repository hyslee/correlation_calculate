#!/bin/python3

from dataclasses import dataclass
from collections import defaultdict
import concurrent.futures
import itertools
import os, sys, re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# 1. Extract atom data from dump file
# 2. Put atoms in a separate group based on the coordinates of the atoms
# 3. calculate the GSFE for each grid
# 4. calculate the correlation 

@dataclass
class SFE:
    def __call__():
        pass

def scanDirs(path):
    dirList = []
    with os.scandir(path) as it:
        for entry in it:
            if not entry.name.startswith('.') and not entry.is_dir():
                dirList.append(entry.name)
    return dirList;

def dumpFileNameInDict(fileNames):
    # Categorize USF, ISF, MSF
    EnergyPosition = defaultdict(list)
    for name in fileNames:
        # Find the word that comes right after '_' string
        parse = re.search(r'(?<=_)\w+', name)
        match parse.group(0):
            case '0':
                EnergyPosition['0'].append(name)
            case 'USF':
                EnergyPosition['USF'].append(name)
            case 'ISF':
                EnergyPosition['ISF'].append(name)
            case 'MSF':
                EnergyPosition['MSF'].append(name)
    return EnergyPosition;

def calcCorrelations():
    return 0;


def returnGroupEnergy1(dataPath, dumpFile, prefix, alloy):
    alats = {'AlMg5': 4.053, 'AlMg10': 4.076, 'AlMg15': 4.0988}

    # Load atomNum
    atomNumLine = 3
    atomNum = np.loadtxt(dataPath+dumpFile, skiprows=atomNumLine, max_rows=1, dtype=int)

    # Load atom data
    dataLabelLine = 9
    atomList = np.loadtxt(dataPath+dumpFile, skiprows=dataLabelLine)
    #f = open('atoms.txt','w')
    #for m in atomList:
    #    f.write(str(m)+'\n')
    #f.close()

    # Read box boundary coordinates
    boxLabelLine = 5
    boxBounds = np.loadtxt(dataPath+dumpFile, skiprows=boxLabelLine, max_rows=3)

    f = open('systeminfo.txt','w')
    f.write(f'atomNum = {atomNum}\n')
    f.write(f'boxBounds = {boxBounds}\n')

    # calculate the box size
    boxSize = [x.sum() for x in np.abs(boxBounds)]

# 1. organize atoms x and y, we have 20 tiles in x and 30 tiles in y axis
#  (10 tiles in -x and 10 tiles in +x), (15 tiles in -y and 15 tiles in +y)
#   --->x+
#   |    ________________________________________________
#   y+  |          |           |           |            |
#       |          |           |           |            | ...
#       |   0-0    |    1-0    |    2-0    |            |
#       |          |  a[112]   |           |            |
#       |__________|___________|___________|____________|
#       |          |           |           |            |
#       |   0-1    |           |           |            |
#       |   a[110] |           |           |            |
#       |          |           |           |            |
#       |__________|___________|___________|___________ |
#       |          |           | (x0,y0)   |            |
#       |          |           |           |            |
#       |   0-2    |           |           |            |
#       |          |           |           |            |
#       |__________|___________|___________|____________|
#       |          |           |           |            |
#       |          |           |           |            |
#       |          |           |           |            |
#       |          |           |           |            |
#       |__________|___________|___________|____________|
#
#                               :
#                               :

# index: 0=id, 1=type, 2=x, 3=y, 4=z, 5=c_pePa, 6=c_kePa, ...

    xlatdist = np.sqrt((-1)**2 + 1**2 + 2**2)
    ylatdist = np.sqrt(1**2 + 1**2 + 0**2)

    #boxLength_x = a['AlMg5']*xlatdist
    #boxLength_y = a['AlMg5']*ylatdist

    boxLength_x = alats[alloy]*xlatdist
    boxLength_y = alats[alloy]*ylatdist

    f.write(f'boxLength_x = {boxLength_x}\n')
    f.write(f'boxLength_y = {boxLength_y}\n')
    f.close()

    # set the coordinate of the first group
    group_start_x = boxBounds[0][0]
    group_start_y = boxBounds[1][0]
    #print(f'{group_start_x} {group_start_y}')
    
    group_end_x = boxBounds[0][1]
    group_end_y = boxBounds[1][1]
    #print(f'{group_end_x} {group_end_y}')

    groupCount_x = 0
    groupCount_y = 0

    # create a list of x axis bounary and y axis boundary
    xboundaries = []
    while group_start_x < group_end_x:
        group_start_x += boxLength_x
        xboundaries.append(group_start_x)

    yboundaries = []
    while group_start_y < group_end_y:
        group_start_y += boxLength_y
        yboundaries.append(group_start_y)
    #
    #print(f'{xboundaries} {yboundaries}')
    #
    re = itertools.cycle(xboundaries)
    re2 = itertools.cycle(yboundaries)
    grids = []
    print(re)
    count1 = 0
    count2 = 0
    for i in re:
        count2 = 0
        for j in re2:
            grids.append([i,j])
            if count2 == len(yboundaries)-1: # index start from 0
                break;
            count2 += 1
        if count1 == len(xboundaries)-1: # index start from 0
            break;
        count1 += 1
    #
    ## just for debugging
    f = open('grid.txt','w')
    for k in grids:
        f.write(str(k)+'\n')
    f.close()

    groupNameCount = 0
    group = defaultdict(list)
    for grid in grids:
        groupNameCount += 1
        for atom in atomList:
            if grid[0]-boxLength_x < atom[2] < grid[0] and grid[1]-boxLength_y < atom[3] < grid[1]:
                # append potential energy of each atom
                group[str(grid[0])+' '+str(grid[1])].append(atom[5])
        print(f'{groupNameCount}')

    f = open(prefix+alloy+'EnergiesPerGroup.txt', 'w')
    totalNum = 0
    totalE = 0
    coordinateE = defaultdict(dict)
    for k, v in group.items():
        # sum all the potential energy of atoms
        groupEnergy = np.array(v).sum()
        f.write(str(k)+' : '+str(groupEnergy)+'\n')
        coordinateE[str(k)] = groupEnergy
        totalNum += len(v)
        totalE += groupEnergy
    f.write(f'totalNum = {totalNum}, totalE = {totalE}')
    f.close()
    return coordinateE;


def returnGroupEnergy(dataPath, dumpFile, prefix):
    alats = {'AlMg5': 4.053, 'AlMg10': 4.076, 'AlMg15': 4.0988}

    for alloy, alat in alats.items():
        # Load atomNum
        atomNumLine = 3
        atomNum = np.loadtxt(dataPath+dumpFile, skiprows=atomNumLine, max_rows=1, dtype=int)

        # Load atom data
        dataLabelLine = 9
        atomList = np.loadtxt(dataPath+dumpFile, skiprows=dataLabelLine)
        #f = open('atoms.txt','w')
        #for m in atomList:
        #    f.write(str(m)+'\n')
        #f.close()

        # Read box boundary coordinates
        boxLabelLine = 5
        boxBounds = np.loadtxt(dataPath+dumpFile, skiprows=boxLabelLine, max_rows=3)

        f = open('systeminfo.txt','w')
        f.write(f'atomNum = {atomNum}\n')
        f.write(f'boxBounds = {boxBounds}\n')

        # calculate the box size
        boxSize = [x.sum() for x in np.abs(boxBounds)]

    # 1. organize atoms x and y, we have 20 tiles in x and 30 tiles in y axis
    #  (10 tiles in -x and 10 tiles in +x), (15 tiles in -y and 15 tiles in +y)
    #   --->x+
    #   |    ________________________________________________
    #   y+  |          |           |           |            |
    #       |          |           |           |            | ...
    #       |   0-0    |    1-0    |    2-0    |            |
    #       |          |  a[112]   |           |            |
    #       |__________|___________|___________|____________|
    #       |          |           |           |            |
    #       |   0-1    |           |           |            |
    #       |   a[110] |           |           |            |
    #       |          |           |           |            |
    #       |__________|___________|___________|___________ |
    #       |          |           | (x0,y0)   |            |
    #       |          |           |           |            |
    #       |   0-2    |           |           |            |
    #       |          |           |           |            |
    #       |__________|___________|___________|____________|
    #       |          |           |           |            |
    #       |          |           |           |            |
    #       |          |           |           |            |
    #       |          |           |           |            |
    #       |__________|___________|___________|____________|
    #
    #                               :
    #                               :

    # index: 0=id, 1=type, 2=x, 3=y, 4=z, 5=c_pePa, 6=c_kePa, ...

        xlatdist = np.sqrt((-1)**2 + 1**2 + 2**2)
        ylatdist = np.sqrt(1**2 + 1**2 + 0**2)

        #boxLength_x = a['AlMg5']*xlatdist
        #boxLength_y = a['AlMg5']*ylatdist

        boxLength_x = alat*xlatdist
        boxLength_y = alat*ylatdist

        f.write(f'boxLength_x = {boxLength_x}\n')
        f.write(f'boxLength_y = {boxLength_y}\n')
        f.close()

        # set the coordinate of the first group
        group_start_x = boxBounds[0][0]
        group_start_y = boxBounds[1][0]
        #print(f'{group_start_x} {group_start_y}')
        
        group_end_x = boxBounds[0][1]
        group_end_y = boxBounds[1][1]
        #print(f'{group_end_x} {group_end_y}')

        groupCount_x = 0
        groupCount_y = 0

        # create a list of x axis bounary and y axis boundary
        xboundaries = []
        while group_start_x < group_end_x:
            group_start_x += boxLength_x
            xboundaries.append(group_start_x)

        yboundaries = []
        while group_start_y < group_end_y:
            group_start_y += boxLength_y
            yboundaries.append(group_start_y)
        #
        #print(f'{xboundaries} {yboundaries}')
        #
        re = itertools.cycle(xboundaries)
        re2 = itertools.cycle(yboundaries)
        grids = []
        print(re)
        count1 = 0
        count2 = 0
        for i in re:
            count2 = 0
            for j in re2:
                grids.append([i,j])
                if count2 == len(yboundaries)-1: # index start from 0
                    break;
                count2 += 1
            if count1 == len(xboundaries)-1: # index start from 0
                break;
            count1 += 1
        #
        ## just for debugging
        f = open('grid.txt','w')
        for k in grids:
            f.write(str(k)+'\n')
        f.close()

        groupNameCount = 0
        group = defaultdict(list)
        for grid in grids:
            groupNameCount += 1
            for atom in atomList:
                if grid[0]-boxLength_x < atom[2] < grid[0] and grid[1]-boxLength_y < atom[3] < grid[1]:
                    # append potential energy of each atom
                    group[str(grid[0])+', '+str(grid[1])].append(atom[5])
            print(f'{groupNameCount}')

        f = open(prefix+alloy+'EnergiesPerGroup.txt', 'w')
        totalNum = 0
        totalE = 0
        coordinateE = defaultdict(list)
        for k, v in group.items():
            # sum all the potential energy of atoms
            groupEnergy = np.array(v).sum()
            f.write(str(k)+' : '+str(groupEnergy)+'\n')
            coordinateE[str(k)+' '+str(v)].append(groupEnergy)
            totalNum += len(v)
            totalE += groupEnergy
        f.write(f'totalNum = {totalNum}, totalE = {totalE}')
        f.close()
        return coordinateE;


def returnEquilStrucName(dataPath, fileNames, Epos):
    fileNames = scanDirs(dataPath)

    # returns dumpfile names in dictionary, key: Energy position, value: list of timesteps
    EnergyPosition = dumpFileNameInDict(fileNames)

    # Load ISF dumpfile names
    data = EnergyPosition[Epos]
    
    # extract timesteps of dumpfiles based on the filename
    dumpTimesteps = np.array( [ int(x.split('.')[1]) for x in data ] )

    # find the dumpfile that has the max timestep (equilibrium)
    tmaxIndex = dumpTimesteps.argmax()

    # declare dumpfile name for equilibrium
    dumpFileName = data[tmaxIndex]
    return dumpFileName;

def returnboxCoordinates(dataPath, dumpFile, alloy):
    alats = {'AlMg5': 4.053, 'AlMg10': 4.076, 'AlMg15': 4.0988}
    # Read box boundary coordinates
    boxLabelLine = 5
    boxBounds = np.loadtxt(dataPath+dumpFile, skiprows=boxLabelLine, max_rows=3)

    # calculate the box size
    boxSize = [x.sum() for x in np.abs(boxBounds)]

    xlatdist = np.sqrt((-1)**2 + 1**2 + 2**2)
    ylatdist = np.sqrt(1**2 + 1**2 + 0**2)

    boxLength_x = alats[alloy]*xlatdist
    boxLength_y = alats[alloy]*ylatdist

    # set the coordinate of the first group
    group_start_x = boxBounds[0][0]
    group_start_y = boxBounds[1][0]
    #print(f'{group_start_x} {group_start_y}')
    
    group_end_x = boxBounds[0][1]
    group_end_y = boxBounds[1][1]
    #print(f'{group_end_x} {group_end_y}')

    groupCount_x = 0
    groupCount_y = 0

    # create a list of x axis bounary and y axis boundary
    xboundaries = []
    while group_start_x < group_end_x:
        group_start_x += boxLength_x
        xboundaries.append(group_start_x)

    yboundaries = []
    while group_start_y < group_end_y:
        group_start_y += boxLength_y
        yboundaries.append(group_start_y)

    return xboundaries, yboundaries;

def main():
    dataPath = '/home/anon/Documents/Research/LAMMPS/SFE_Correlation/2-SFE_Correlation/AlMg5/dumpfiles/'
    # Scan directory and return the list of filenames
    fileNames = scanDirs(dataPath)

    ## returns dumpfile names in dictionary, key: Energy position, value: list of timesteps
    #EnergyPosition = dumpFileNameInDict(fileNames)

    ## Load ISF dumpfile names
    #ISFdata = EnergyPosition['ISF']
    #USFdata = EnergyPosition['USF']
    #
    ## extract timesteps of dumpfiles based on the filename
    #dumpTimesteps = np.array( [ int(x.split('.')[1]) for x in ISFdata ] )

    ## find the dumpfile that has the max timestep (equilibrium)
    #tmaxIndex = dumpTimesteps.argmax()

    ## declare dumpfile name for equilibrium
    #dumpFile = ISFdata[tmaxIndex]

    # filesnames
    dumpISF = returnEquilStrucName(dataPath, fileNames, 'ISF')
    dumpFCC = returnEquilStrucName(dataPath, fileNames, '0')

    # lattice parameters
    alat = {'AlMg5': 4.053, 'AlMg10': 4.076, 'AlMg15': 4.0988}

    # Load atomNum
    atomNumLine = 3
    atomNum = np.loadtxt(dataPath+dumpFCC, skiprows=atomNumLine, max_rows=1, dtype=int)

    #FCCPotential = returnGroupEnergy1(dataPath, dumpFCC, 'FCC', 'AlMg5')
    #ISFPotential = returnGroupEnergy1(dataPath, dumpFCC, 'ISF', 'AlMg5')

    # Do the job in parallel
    with concurrent.futures.ProcessPoolExecutor() as executor:
        p1 = executor.submit(returnGroupEnergy1, dataPath, dumpFCC, 'FCC', 'AlMg5')
        p2 = executor.submit(returnGroupEnergy1, dataPath, dumpISF, 'ISF', 'AlMg5')
        FCCPotential = p1.result()
        ISFPotential = p2.result()
    print(f'FCCPotential = {FCCPotential}')
    print(FCCPotential['-89.24324444895525 -80.15415529136448'])
    #print(ISFPotential)
    
    for (k, v), (k2, v2) in zip(ISFPotential.items(), FCCPotential.items()):
        print(f'{k} {v}')
        ISFPotential[k] = v - v2

    df = pd.DataFrame(columns=['x', 'y', 'E'])
    # loop through some data and append rows to the DataFrame
    for k, v in ISFPotential.items():
        k = np.array([float(x) for x in k.split()])
        row = {'x': k[0], 'y': k[1], 'E': v}
        #df = df.concat(row, ignore_index=True)
        df = pd.concat([df, pd.DataFrame(row, index=[0])], ignore_index=True)
    print(df)

    # create meshgrid for contour plot
    x = df['x'].unique()
    y = df['y'].unique()
    X, Y = np.meshgrid(x, y)

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    #ax.plot(x, y)
    ax.scatter(X, Y)
    plt.savefig('xy.pdf')
    plt.show()
    exit()

    xcord, ycord = returnboxCoordinates(dataPath, dumpFile, alloy)
    # create z values for contour plot
    Z = df.pivot(index='y', columns='x', values='E').values

    # plot contour map
    
    plt.contour(X, Y, Z)

    # add labels and title
    plt.xlabel('X-axis')
    plt.ylabel('Y-axis')
    plt.title('GSFE')

    # display the plot
    plt.savefig('AlMg5Contour.pdf')
    plt.show()
    exit()


if __name__ == '__main__':
    main()
