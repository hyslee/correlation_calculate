
    def calAutoCor(self, GFE):
        dx_limit = int(len(GFE[0,:])/2)
        dy_limit = int(len(GFE[:,0])/2)
        # x correlation, only in x direction (1step at a time when y direction)
        #dx_limit = int(len(xRange)/2)
        #dy_limit = int(len(yRange)/2)
        dxx = np.arange(1, dx_limit+1)
        dyy = np.arange(1, dy_limit+1)
        #rc = 0 # accumulator

        # single row test in x direction
        rcoeff = []
        Nx = len(GFE[0,:]) # number of element in x
        Ny = len(GFE[:,0]) # number of element in y

        #Nx = len(xRange) # number of element in x
        #Ny = len(yRange) # number of element in y
        for dx in dxx:
            rc = 0
            for x in range(Nx):
                # sets the ref point based on x,y and iterate over x with dx
                x1 = x  
                # periodic condition
                x2 = x + dx if x+dx <= Nx-1 else x + dx - Nx
                #r = GSFEmesh[x1][0] * GSFEmesh[x2][0]
                r = GFE[0, x1] * GFE[0, x2]
                rc += r
            rcoeff.append(rc/Nx)
        print(f'(single row) rcoeff = {rcoeff}')
        print(f'avg GFE = {np.mean(GFE[0])}')

        # multiple row test in x direction
        # Check if we need to average them or not
        # tiling is another option
        rcoeff = []
        #Nx = len(xRange) # number of element in x
        #Ny = len(yRange) # number of element in y
        # iterate d with varying delx
        for dx in dxx:
            rc2 = 0 # accumulator2
            # iterate along column (y axis)
            for y in range(Ny):
                rc = 0 # acuumulator1
                # iterate along row (x axis)
                for x in range(Nx):
                    # sets the ref point based on x,y and iterate over x with dx
                    x1 = x  
                    # periodic condition
                    x2 = x + dx if x+dx <= Nx-1 else x + dx - Nx
                    #r = GSFEmesh[x1][y] * GSFEmesh[x2][y]
                    r = GFE[y, x1] * GFE[y, x2]
                    rc += r
                rc2 += rc/Nx
            rcoeff.append(rc2/Ny)
        print(f'(multiple rows) rcoeff = {rcoeff}')

        # single row test in y direction
        rcoeff = []
        #Nx = len(xRange) # number of element in x
        #Ny = len(yRange) # number of element in y
        for dy in dyy:
            rc = 0
            for y in range(Ny):
                y1 = y  
                # periodic condition
                y2 = y+dy if y+dy <= Ny-1 else y+dy-Ny
                # random var product
                r = GFE[y1,0]*GFE[y2,0]
                # sum product
                rc += r
            rcoeff.append(rc/Ny)
        print(f'(single row y) rcoeff = {rcoeff}')

        # multiple row test in x direction
        # Check if we need to average them or not
        # tiling is another option
        rcoeff = []
        #Nx = len(xRange) # number of element in x
        #Ny = len(yRange) # number of element in y
        # iterate d with varying delx
        for dy in dyy:
            rc2 = 0 # accumulator2
            # iterate along column (y axis)
            for x in range(Nx):
                rc = 0 # acuumulator1
                # iterate along row (x axis)
                for y in range(Ny):
                    # sets the ref point based on x,y and iterate over x with dx
                    y1 = y  
                    # periodic condition
                    y2 = y+dy if y+dy <= Ny-1 else y+dy-Ny
                    r = GFE[y1, x] * GFE[y2, x]
                    rc += r
                rc2 += rc/Ny
            rcoeff.append(rc2/Nx)
        print(f'(multiple rows y) rcoeff = {rcoeff}')
