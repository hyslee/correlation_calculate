#!/bin/python3

#######################################
# Draw the number of atoms on each grid
#######################################

import sys, os
import numpy as np

def main():
    try:
        userInputIndex = 1
        seedInputIndex = 2
        alloy = sys.argv[userInputIndex]
        seed = sys.argv[seedInputIndex]
    except IndexError:
        print('you need to provide an argument,\n1st arg: AlMg5, AlMg10, AlMg15,\n2snd arg: seedNumber')
        sys.exit(1)

    match alloy:
        case 'AlMg5':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg5_v2/AlMg5_seed{seed}/dumpfiles/'
            dataPath2 = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg5_s{seed}/debugFiles/'
        case 'AlMg10':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg10/AlMg10_seed{seed}/dumpfiles/'
            dataPath2 = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg10_s{seed}/debugFiles/'
        case 'AlMg15':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg15/AlMg15_seed{seed}/dumpfiles/'
            dataPath2 = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg15_s{seed}/debugFiles/'
        case _:
            print('Invalid Argument')
            sys.exit(1)
 
    np.loadtxt('atomNum_bin.txt', skiprows=)
    #with open('meshTest.txt', encoding='utf-8') as f:
    #    f.write()


    return 0;

if __name__ == "__main__":
    main()
