#!/bin/python3

import numpy as np
import re, sys, os
from dataclasses import dataclass

@dataclass
class rawAtomData:
    alloy : str
    dataPath : str
    debug : int = 0

    def scanDirs(self, path):
        dirList = []
        with os.scandir(path) as it:
            for entry in it:
                if not entry.name.startswith('.') and not entry.is_dir():
                    dirList.append(entry.name)
        return dirList;

    # takes dump path and energy position (USF, ISF..) as arguments
    def loadDumpData(self, Epos: str):
        # scan filenames in "dataPath" directory
        fileNames = self.scanDirs(self.dataPath)

        # extract Energy position files
        filesAtEpos = []
        for name in fileNames:
            # Find the word that comes right after '_' string
            parse = re.search(r'(?<='+self.alloy+'_)'+Epos, name)
            if parse:
                filesAtEpos.append(name)

        # find the dumpfile that has the max timestep (equilibrium)
        equilTime = np.array( [ int(x.split('.')[1]) for x in filesAtEpos ] ).max()

        # dump file name that will be loaded (ex) AlMg5_0.5)
        dumpFile = self.alloy+'_'+Epos+'.'+str(equilTime)

        # Load Sim box info
        #boxLabelLineStart = 5
        #boxInfo = np.loadtxt(dataPath+dumpFile, skiprows=boxLabelLineStart, max_rows=3)

        # Load atom data
        dataLabelLine = 9
        atomList = np.loadtxt(self.dataPath+dumpFile, skiprows=dataLabelLine)

        # Return atom data
        return atomList;
