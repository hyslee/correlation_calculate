#!/bin/gnuplot

# read user inputs
alloy = ARG1
seed = ARG2
dataName = ARG3

# png image set
set terminal pngcairo  transparent enhanced font "Times-New-Roman, 25" fontscale 1.0 size 1000, 1000

# pdf image set
#set terminal pdfcairo  transparent enhanced font "Times-New-Roman, 40" fontscale 1.0 size 2000, 1800 

# set output filename
outName = sprintf("figures/GSFE_heatmap_%s_%s.png", alloy, seed)
set output outName

# Various ways to create a 2D heat map from ascii data
titleName = sprintf("GSFE %s seed %s", alloy, seed)
set title titleName
unset key
set tic scale 0

# Color runs from white to green
set palette rgbformula 21,22,23

# set heat range
#set cbrange [0:5]

set cblabel "Energy [mJ/m^2]"
set cbtics

set xrange [0:9]
set yrange [0:19]
set xlabel "x[112]"
set ylabel "y[110]"

set view map

datName = sprintf("src/%s.txt", dataName)
splot datName matrix with image

