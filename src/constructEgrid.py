#!/bin/python3

import numpy as np
import numba as nb

@nb.njit(cache=True)
def constructEgrid(nx, ny):
    grid = np.zeros((nx+1, ny+1))
    return grid;

