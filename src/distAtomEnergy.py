#!/bin/python3

import sys
import numpy as np
import numba as nb

@nb.njit(cache=True, parallel=True)
def calcGridEnergy(atomData, Nx, Ny, Egrid, xGridWidth_new, yGridWidth_new):
    ## array indexes for atom data
    idIndex = 0
    xAxisIndex = 2
    yAxisIndex = 3
    PEIndex = 5 # potential energy
    KEIndex = 6 # kinetic energy
    ##############################
    Atotal = xGridWidth_new * yGridWidth_new # gridSize

    #f = open('indexdebug.txt', 'w')
    for atom in atomData:
        atomE = atom[PEIndex]+atom[KEIndex]
        atomXpos = atom[xAxisIndex]
        atomYpos = atom[yAxisIndex] 
        #print(f'atomXpos = {atomXpos} atomYpos = {atomYpos}')

        xIndex = int(np.floor( atomXpos/xGridWidth_new ))
        yIndex = int(np.floor( atomYpos/yGridWidth_new ))
        #f.write(f'xIndex = {xIndex} yIndex = {yIndex}\n')

        point1 = (xIndex, yIndex)
        point2 = (xIndex+1, yIndex)
        point3 = (xIndex, yIndex+1)
        point4 = (xIndex+1, yIndex+1)
        x, y = 0, 1
        #print(f'point1 = {point1} point2 = {point2} point3 = {point3} point4 = {point4}')

        point1Coord = (xGridWidth_new*point1[x], yGridWidth_new*point1[y])
        point2Coord = (xGridWidth_new*point2[x], yGridWidth_new*point2[y])
        point3Coord = (xGridWidth_new*point3[x], yGridWidth_new*point3[y])
        point4Coord = (xGridWidth_new*point4[x], yGridWidth_new*point4[y])
        #f.write(f'point1Coord = {point1Coord} point2Coord = {point2Coord} point3Coord = {point3Coord} point4Coord = {point4Coord} \n')
        #print(f'{point1Coord} {point2Coord} {point3Coord} {point4Coord}')

        A1frac = np.abs(atomXpos-point1Coord[x]) * np.abs(atomYpos-point1Coord[y])
        A2frac = np.abs(atomXpos-point2Coord[x]) * np.abs(atomYpos-point2Coord[y])
        A3frac = np.abs(atomXpos-point3Coord[x]) * np.abs(atomYpos-point3Coord[y])
        A4frac = np.abs(atomXpos-point4Coord[x]) * np.abs(atomYpos-point4Coord[y])
        #f.write(f'A1frac = {A1frac} A2frac = {A2frac} A3frac = {A3frac} A4frac = {A4frac}\n')

        #print(f'{A1frac+A2frac+A3frac+A4frac}')
        #print(f'{A1frac} {A2frac} {A3frac} {A4frac}')

        w1 = A4frac/Atotal
        w2 = A3frac/Atotal
        w3 = A2frac/Atotal
        w4 = A1frac/Atotal

        E1 = w1*atomE
        E2 = w2*atomE
        E3 = w3*atomE
        E4 = w4*atomE

        # periodic condition
        if xIndex < 0:
            xIndex = xIndex + Nx
        if xIndex > Nx:
            xIndex = xIndex - Nx
        if yIndex < 0:
            yIndex = yIndex + Ny
        if yIndex > Ny:
            yIndex = yIndex - Ny
        point1 = (xIndex, yIndex)
        point2 = (xIndex+1, yIndex)
        point3 = (xIndex, yIndex+1)
        point4 = (xIndex+1, yIndex+1)

        Egrid[point1[x], point1[y]] += E1
        Egrid[point2[x], point2[y]] += E2
        Egrid[point3[x], point3[y]] += E3
        Egrid[point4[x], point4[y]] += E4

        #       p3                      p4
        #       ------------------------
        #       |          |           |
        #       |          |           |
        #       |          |           |
        #       |__________|___________|
        #       |          |           |
        #       |          |           |
        #       |          |           |
        # ^ x+  |__________|___________|
        # |     p1                      p2
        #   ->y+

    #periodicity
    Egrid[:,0] = Egrid[:,0]+Egrid[:,-1]
    Egrid[0] = Egrid[0]+Egrid[-1]

    # make the last gridpoints the same (periodicity)
    #Egrid[-1] = Egrid[0]
    #Egrid[:,-1] = Egrid[:,0]

    #remove last gridpoints
    Egrid = Egrid[0:Nx, 0:Ny]

    return Egrid;

@nb.njit(cache=True, parallel=True)
def calcGSFE(atomData, faultedAtomData, Nx, Ny, Egrid, xGridWidth_new, yGridWidth_new):
    ## array indexes for atom data
    idIndex = 0
    xAxisIndex = 2
    yAxisIndex = 3
    PEIndex = 5 # potential energy
    KEIndex = 6 # kinetic energy
    ##############################
    # index correction (start from 0)
    Nx = Nx-1
    Ny = Ny-1

    # make negative coordinate to zero
    shftAtom_x = np.abs(np.amin(atomData[:, xAxisIndex]))
    shftAtom_y = np.abs(np.amin(atomData[:, yAxisIndex]))

    # gridsize
    Atotal = xGridWidth_new * yGridWidth_new 
    Atotal_SI = xGridWidth_new*1e-10 * yGridWidth_new*1e-10

    #f = open('indexdebug.txt', 'w')
    #count = 0
    for atom in atomData:
        #count += 1

        atomE = atom[PEIndex]+atom[KEIndex]
        atomXpos = atom[xAxisIndex]+shftAtom_x
        atomYpos = atom[yAxisIndex]+shftAtom_y
        perfectAtomID = atom[idIndex]

        # find the same atom in the faulted crystal data
        for faultedAtom in faultedAtomData:
            if faultedAtom[idIndex] == perfectAtomID:
                faultedAtomE = faultedAtom[PEIndex]+faultedAtom[KEIndex]

        xIndex = int(np.floor( atomXpos/xGridWidth_new ))
        yIndex = int(np.floor( atomYpos/yGridWidth_new ))
        #f.write(f'xIndex = {xIndex} yIndex = {yIndex}\n')

        point1 = (xIndex, yIndex)
        point2 = (xIndex+1, yIndex)
        point3 = (xIndex, yIndex+1)
        point4 = (xIndex+1, yIndex+1)
        x, y = 0, 1
        #f.write(f'point1 = {point1} point2 = {point2} point3 = {point3} point4 = {point4}')

        point1Coord = (xGridWidth_new*point1[x], yGridWidth_new*point1[y])
        point2Coord = (xGridWidth_new*point2[x], yGridWidth_new*point2[y])
        point3Coord = (xGridWidth_new*point3[x], yGridWidth_new*point3[y])
        point4Coord = (xGridWidth_new*point4[x], yGridWidth_new*point4[y])
        #f.write(f'point1Coord = {point1Coord} point2Coord = {point2Coord} point3Coord = {point3Coord} point4Coord = {point4Coord} \n')

        A1frac = np.abs(atomXpos-point1Coord[x]) * np.abs(atomYpos-point1Coord[y])
        A2frac = np.abs(atomXpos-point2Coord[x]) * np.abs(atomYpos-point2Coord[y])
        A3frac = np.abs(atomXpos-point3Coord[x]) * np.abs(atomYpos-point3Coord[y])
        A4frac = np.abs(atomXpos-point4Coord[x]) * np.abs(atomYpos-point4Coord[y])
        #f.write(f'A1frac = {A1frac} A2frac = {A2frac} A3frac = {A3frac} A4frac = {A4frac}\n')

        w1 = A4frac/Atotal
        w2 = A3frac/Atotal
        w3 = A2frac/Atotal
        w4 = A1frac/Atotal
        #f.write(f'w1 w2 w3 w4 = {w1} {w2} {w3} {w4}\n')

        # generalize the energy
        eV_to_mJ = 1.602176633e-16
        #convert the units
        faultedAtomE = faultedAtomE*eV_to_mJ
        atomE = atomE*eV_to_mJ
        atomE = (faultedAtomE - atomE) / Atotal_SI

        E1 = w1*atomE
        E2 = w2*atomE
        E3 = w3*atomE
        E4 = w4*atomE
        #f.write(f'E1 E2 E3 E4 = {E1} {E2} {E3} {E4}')

        Egrid[point1[x], point1[y]] += E1
        Egrid[point2[x], point2[y]] += E2
        Egrid[point3[x], point3[y]] += E3
        Egrid[point4[x], point4[y]] += E4

        # periodic condition
        firstXgrid = 0
        firstYgrid = 0
        lastXgrid = -1
        lastYgrid = -1
        if xIndex==0 and yIndex==0:
            Egrid[lastXgrid, firstYgrid] += E1
            Egrid[firstXgrid, lastYgrid] += E1
            Egrid[lastXgrid, lastYgrid] += E1
            Egrid[1, lastYgrid] += E2
            Egrid[lastXgrid, 1] += E3

        elif xIndex==Nx and yIndex==0:
            Egrid[point1[x], lastYgrid] += E1
            Egrid[firstXgrid, firstYgrid] += E2
            Egrid[lastXgrid, lastYgrid] += E2
            Egrid[firstXgrid, lastYgrid] += E2
            Egrid[0, point4[y]] += E4

        elif xIndex==0 and yIndex==Ny:
            Egrid[lastXgrid, point1[y]] += E1
            Egrid[firstXgrid, firstYgrid] += E3
            Egrid[lastXgrid, lastYgrid] += E3
            Egrid[lastXgrid, firstYgrid] += E3
            Egrid[point4[x], firstYgrid] += E4

        elif xIndex==Nx and yIndex==Ny:
            Egrid[firstXgrid, point2[y]] += E2
            Egrid[point3[x], firstYgrid] += E3
            Egrid[firstXgrid, firstYgrid] += E4
            Egrid[lastXgrid, firstYgrid] += E4
            Egrid[firstXgrid, lastYgrid] += E4

        elif xIndex==0:
            Egrid[lastXgrid, point1[y]] += E1
            Egrid[lastXgrid, point3[y]] += E3

        elif yIndex==0:
            Egrid[point1[x], lastYgrid] += E1
            Egrid[point2[x], lastYgrid] += E2

        elif xIndex==Nx:
            Egrid[firstXgrid, point2[y]] += E2
            Egrid[firstXgrid, point4[y]] += E4

        elif yIndex==Ny:
            Egrid[point3[x], firstYgrid] += E3
            Egrid[point4[x], firstYgrid] += E4

        #       p3                      p4
        #       ------------------------
        #       |          |           |
        #       |          |           |
        #       |          |           |
        #       |__________|___________|
        #       |          |           |
        #       |          |           |
        #       |          |           |
        # ^ x+  |__________|___________|
        # |     p1                      p2
        #   ->y+

        # for debugging
        #np.savetxt(f'./debugData/gridE.{count}', Egrid)

    return Egrid;
