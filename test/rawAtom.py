#!/bin/python3

import numpy as np
import re, os
import numba as nb

def scanDirs(path) -> list:
    dirList = []
    with os.scandir(path) as it:
        for entry in it:
            if not entry.name.startswith('.') and not entry.is_dir():
                dirList.append(entry.name)
    return dirList;

def findMinimumEStructName(fileNameList: str, alloy: str, Epos: str) -> str:
    # extract Energy position files
    filesAtEpos = []
    for name in fileNameList:
        # Find the word that comes right after '_' string
        parse = re.search(r'(?<='+alloy+'_)'+Epos, name)
        if parse:
            filesAtEpos.append(name)

    # find the dumpfile that has the max timestep (equilibrium)
    equilTime = np.amax(np.array( [ int(x.split('.')[1]) for x in filesAtEpos ] ))

    # dump file name that will be loaded (ex) AlMg5_0.5)
    dumpFileName = alloy+'_'+Epos+'.'+str(equilTime)
    return dumpFileName;

def loadAtomDat(dataPath: str, dumpFile: str) -> np.ndarray:
    # Load atom data
    atomDataStartLine = 9
    atomList = np.loadtxt(dataPath+dumpFile, skiprows=atomDataStartLine)

    # Return atom data
    return atomList;

