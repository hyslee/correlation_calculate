#!/bin/python3

import sys
import numpy as np
import numba as nb

@nb.njit(cache=True, parallel=True)
def corrEnergy(atomData, atomIDinBins, xGridWidth, yGridWidth) -> np.ndarray:
    ## array indexes for atom data
    idIndex = 0
    xAxisIndex = 2
    yAxisIndex = 3
    PEIndex = 5 # potential energy
    KEIndex = 6 # kinetic energy
    ##############################

    # find the lowest atom coordinate
    xlow = np.amin(atomData[:, xAxisIndex])
    ylow = np.amin(atomData[:, yAxisIndex])

    # find the highest atom coordinate
    xhigh = np.amax(atomData[:, xAxisIndex])
    yhigh = np.amax(atomData[:, yAxisIndex])

    # Set grid start and end points
    xStart = xlow + xGridWidth
    xEnd = xhigh + xGridWidth
    yStart = ylow + yGridWidth
    yEnd = yhigh + yGridWidth

    # grid range
    xRange = np.arange(xStart, xEnd, xGridWidth)
    yRange = np.arange(yStart, yEnd, yGridWidth)

    # grid Size
    convFactorAngToMeter = 1e-10
    gridSize_SI = xGridWidth*convFactorAngToMeter * yGridWidth*convFactorAngToMeter

    # initialize the energy grid
    Egrid = np.zeros((len(xRange)+1, len(yRange)+1))

    # go through the grid one by one
    #for atomID in atomIDinBins:
    #    for atom in atomID:
    #        # if the value is not zero
    #        if atom:
    #            for fullAtomdata in atomData:
    #            atomData
    

    # scan atom and cal col index and row index
    #i = 0
    #for atomID in atomIDinBins:
    #    for atom in atomID:
    #        # if the value is not zero
    #        if atom:
    #            atomData

    return Egrid;
