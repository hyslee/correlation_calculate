#!/bin/python3

import numpy as np
import re, sys, os
import numba as nb
from numba import float64

@nb.njit(cache=True)
def meshRange(latParameter: float, atomDat: np.ndarray, xGridWidth: float, yGridWidth: float) -> np.ndarray:
    ## array indexes for atom data
    idIndex = 0
    xAxisIndex = 2
    yAxisIndex = 3
    PEIndex = 5 # potential energy
    KEIndex = 6 # kinetic energy
    ##############################

    # find the lowest atom coordinate
    xlow = np.amin(atomDat[:, xAxisIndex])
    ylow = np.amin(atomDat[:, yAxisIndex])

    # find the highest atom coordinate
    xhigh = np.amax(atomDat[:, xAxisIndex])
    yhigh = np.amax(atomDat[:, yAxisIndex])

    # Set grid start and end points
    xStart = xlow + xGridWidth
    xEnd = xhigh + xGridWidth
    yStart = ylow + yGridWidth
    yEnd = yhigh + yGridWidth

    # grid range
    xRange = np.arange(xStart, xEnd, xGridWidth)
    yRange = np.arange(yStart, yEnd, yGridWidth)

    return xRange, yRange;
    #return xlow, ylow;
    #return xhigh, yhigh;
