#!/bin/python3

import numpy as np
import re, sys, os
import numba as nb
from numba import int32

@nb.njit(cache=True, parallel=True)
def binAtom(xGridWidth: float, yGridWidth: float, atomData: np.ndarray) -> np.ndarray:
    ## array indexes for atom data
    idIndex = 0
    xAxisIndex = 2
    yAxisIndex = 3
    PEIndex = 5 # potential energy
    KEIndex = 6 # kinetic energy
    ##############################

    # find the lowest atom coordinate
    xlow = np.amin(atomData[:, xAxisIndex])
    ylow = np.amin(atomData[:, yAxisIndex])

    # find the highest atom coordinate
    xhigh = np.amax(atomData[:, xAxisIndex])
    yhigh = np.amax(atomData[:, yAxisIndex])

    # Set grid start and end points
    xStart = xlow + xGridWidth
    xEnd = xhigh + xGridWidth
    yStart = ylow + yGridWidth
    yEnd = yhigh + yGridWidth

    # grid range
    xRange = np.arange(xStart, xEnd, xGridWidth)
    yRange = np.arange(yStart, yEnd, yGridWidth)

    # initialize the atomID bin grid (np.empty prevents parallel=True)
    totalAtomBin = np.zeros((200,100))
    #energyAtomBin = np.zeros((200,100))
    rowCount = 0
    for x in xRange:
        for y in yRange:
            atomBin = np.empty(0)
            columnCount = 0
            for atom in atomData:
                if x == xStart and y == yStart:
                    if atom[xAxisIndex] < x and atom[yAxisIndex] < y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
                elif x == xEnd and y == yStart:
                    if atom[xAxisIndex] > x and atom[yAxisIndex] < y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
                elif x == xStart and y == yEnd:
                    if atom[xAxisIndex] < x and atom[yAxisIndex] > y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
                elif x == xEnd and y == yEnd:
                    if atom[xAxisIndex] > x and atom[yAxisIndex] > y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
                elif x == xStart:
                    if atom[xAxisIndex] < x and y-yGridWidth < atom[yAxisIndex] < y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
                elif x == xEnd:
                    if atom[xAxisIndex] > x and y-yGridWidth < atom[yAxisIndex] < y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
                elif y == yStart:
                    if x-xGridWidth < atom[xAxisIndex] < x and atom[yAxisIndex] < y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
                elif y == yEnd:
                    if x-xGridWidth < atom[xAxisIndex] < x and atom[yAxisIndex] > y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
                else:
                    if x-xGridWidth < atom[xAxisIndex] < x and y-yGridWidth < atom[yAxisIndex] < y:
                        totalAtomBin[rowCount, columnCount] += atom[idIndex]
                        #energyAtomBin[rowCount, columnCount] += atom[PEIndex]+atom[KEIndex]
                        columnCount += 1
                    else:
                        continue;
            rowCount += 1
    # make the array type to integer
    totalAtomBin = totalAtomBin.astype(np.int32)
    #return totalAtomBin, energyAtomBin;
    #return totalAtomBin;
    return xlow, ylow, xhigh, yhigh;

