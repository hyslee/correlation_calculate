#!/bin/python3

import sys
import numpy as np
#import numba as jit, int32

from numba import jit

#@jit(int32(int32, int32), parallel=True)
#def f(x, y):
#    # A somewhat trivial example
#    return x + y

#@jit(nopython=True)
#@jit(int32(), nopython=True)
@jit(nopython=True)
def nptest(arr, arr2):
    #a = np.empty(0, dtype=float)
    #a = a[-1]
    # if arary is availiable
    #a = np.append(a,[1])
    a = np.vstack((arr, arr2))
    return a;

#f(1,2)
a = np.array(([1,2,3],[4,5,6]))
print(a)
print(a[0,2])
