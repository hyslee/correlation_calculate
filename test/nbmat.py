#!/bin/python3

import sys
import numpy as np
#import numba as nb

# custom functions
import rawAtom as ra
import mesh as ms
import energyGrid as eg
import binAtom as bm
import correlatedEnergy as ce

def main():
    ##### handle user input
    try:
        userInputIndex = 1
        seedInputIndex = 2
        alloy = sys.argv[userInputIndex]
        seed = sys.argv[seedInputIndex]
    except IndexError:
        print('you need to provide an argument,\n1st arg: AlMg5, AlMg10, AlMg15,\n2snd arg: seedNumber')
        sys.exit(1)

    match alloy:
        case 'AlMg5':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg5_v2/AlMg5_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg5_s{seed}/'
            latParameter = 4.053
        case 'AlMg10':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg10/AlMg10_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg10_s{seed}/'
            latParameter = 4.076
        case 'AlMg15':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg15/AlMg15_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg15_s{seed}/'
            latParameter = 4.0988
        case _:
            print('Invalid Argument')
            sys.exit(1)
    ############################

    # call filelist inside data directory
    fileList = ra.scanDirs(dataPath)
    
    # call min energy structure fileName
    # 0 : perfect crystal, ISF : intrinsic stacking fault 
    dumpFileName_E0 = ra.findMinimumEStructName(fileList, alloy, Epos='0')

    # load raw atom data (there is no problem)
    atomE0 = ra.loadAtomDat(dataPath, dumpFileName_E0)

    # Grid Parameters
    distToNextAtom112 = np.sqrt((-1)**2 + 1**2 + 2**2)/2
    distToNextAtom110 = np.sqrt(1**2 + 1**2 + 0**2)/2
    xGridWidth = latParameter*distToNextAtom112 
    yGridWidth = latParameter*distToNextAtom110 

    # set Area of each grid in meter (maybe do this inside correlation calculation)
    #convFactorAngToMeter = 1e-10
    #gridSize_SI = xGridWidth*convFactorAngToMeter * yGridWidth*convFactorAngToMeter

    # bin the atoms to generate lookup table for grid energy calculation
    # it returns zeros(empty) array too, so take that into consideration
    #bins = bm.binAtom(xGridWidth, yGridWidth, atomE0)
    x1,y1,x2,y2 =bm.binAtom(xGridWidth, yGridWidth, atomE0) 
    print(f'{x1} {y1} {x2} {y2}')
    exit()

    # energy grid (probably I should do this in the function)
    #meshSize = (len(xRange), len(yRange))
    #Egrid = eg.initEnergyGrid(meshSize)
    #print(Egrid)
    # accumulate energy on each gridpoints
    # just ignore zeros when calculating weight factor
    #Egrid = ce.corrEnergy(atomE0, bins, xGridWidth, yGridWidth)
    #print(Egrid)

    #for j in bins:
    #    print(j)

    # 1. go through the mesh range one by one
    # 2. scan the atoms in the bin, calculate At, Af and weight factor
    # 3. distribute energy of the atom on each grid points
    #       - I should have all the grid point index since I already
    #         initialized it

    #for x in xRange:
    #    for y in yRange:
    #        if atom is in range
    #            eGridIndex1,2,3,4 = 
    #            Afrac = 
    #            Atotal = xGridWidth*yGridWidth
    #            weight1,2,3,4 = 
    #            grid += weight1*Eatom...


    return 0;

if __name__ == '__main__':
    main()

