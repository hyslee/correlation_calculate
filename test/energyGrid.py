import sys
import numpy as np
import numba as nb

@nb.njit(cache=True)
def initEnergyGrid(matrixSize: tuple) -> np.ndarray:
    Egrid = np.zeros(matrixSize)
    return Egrid;

