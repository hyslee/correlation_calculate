#!/bin/python3

import sys, os
import numpy as np
#import numba as nb

# custom functions
sys.path.insert(1,'./src') # add library directory path
import rawAtom as ra
import calNewGridWidth as cn
import constructEgrid as ce
import distAtomEnergy as da
#import plot as pt # matplotlib plotting, may depreciated
import calcCorrelation as cc
import avgCorrData as ac

def main():
    try:
        userInputIndex = 1
        seedInputIndex = 2
        alloy = sys.argv[userInputIndex]
        seed = sys.argv[seedInputIndex]
    except IndexError:
        print('you need to provide an argument,\n1st arg: AlMg5, AlMg10, AlMg15,\n2snd arg: seedNumber')
        sys.exit(1)

    match alloy:
        case 'AlMg5':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg5_v2/AlMg5_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg5_s{seed}/'
            latParameter = 4.053
        case 'AlMg10':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg10/AlMg10_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg10_s{seed}/'
            latParameter = 4.076
        case 'AlMg15':
            dataPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/2-SFE_Correlation/AlMg15/AlMg15_seed{seed}/dumpfiles/'
            outputPath = f'/home/anon/Documents/Research/LAMMPS/SFE_Correlation_Chunk/3-PostProcessing/AlMg15_s{seed}/'
            latParameter = 4.0988
        case _:
            print('Invalid Argument')
            sys.exit(1)
    ############################

    # call filelist inside data directory
    fileList = ra.scanDirs(dataPath)
    
    # call min energy structure fileName
    # 0 : perfect crystal, ISF : intrinsic stacking fault 
    dumpFileName_E0 = ra.findMinimumEStructName(fileList, alloy, Epos='0')
    dumpFileName_EISF = ra.findMinimumEStructName(fileList, alloy, Epos='ISF')
    #print(f'{dumpFileName_E0} {dumpFileName_EISF}')

    # load raw atom data (there is no problem)
    atomE0 = ra.loadAtomDat(dataPath, dumpFileName_E0)
    atomEISF = ra.loadAtomDat(dataPath, dumpFileName_EISF)
    #print(f'{atomE0}\n \n{atomEISF}')

    # load box data
    boxE0 = ra.loadBoxDat(dataPath, dumpFileName_E0)

    # Grid Parameters
    distToNextAtom112 = np.sqrt((-1)**2 + 1**2 + 2**2)/2
    distToNextAtom110 = np.sqrt(1**2 + 1**2 + 0**2)/2
    xGridWidth = latParameter*distToNextAtom112 
    yGridWidth = latParameter*distToNextAtom110 
    #print(f'xGridWidth = {xGridWidth}, yGriddWidth = {yGridWidth}')

    # new grid parameter
    nx, ny, xGridWidth_new, yGridWidth_new = cn.calNewWidth(boxE0, xGridWidth, yGridWidth)
    #print(f'{xGridWidth_new} {yGridWidth_new}')
    #print(f'{nx} {ny}')

    # construct energy grid
    Egrid = ce.constructEgrid(nx, ny)

    # distribute atom energy
    #Egrid = da.calcGridEnergy(atomE0, nx, ny, Egrid, xGridWidth_new, yGridWidth_new)
    
    # distribute and calculate correlated GSFE
    Egrid = da.calcGSFE(atomE0, atomEISF, nx, ny, Egrid, xGridWidth_new, yGridWidth_new)

    # draw heatmap
    # gnuplot plotting
    dataName = "Egrid"
    np.savetxt(f"src/{dataName}.txt", Egrid.T)
    os.system(f"gnuplot -c ./src/plot_heatmap.gp {alloy} {seed} {dataName}")
    os.system(f"rm ./src/{dataName}.txt")

    # calculate correlation
    corXdir, corYdir = cc.calAutoCor(Egrid.T, normalize=1)
    # calculate average correlation data
    avgCorXdir = ac.AVGdata(corXdir)
    avgCorYdir = ac.AVGdata(corYdir)

    # save the dummy data for plotting
    ## plot individual correlation
    dataName1 = "corXdir"
    dataName2 = "corYdir"
    np.savetxt(f"src/{dataName1}.txt", corXdir.T) # you have to transpose your data since gnuplot read columns
    np.savetxt(f"src/{dataName2}.txt", corYdir.T)
    os.system(f"gnuplot -c ./src/plot_correlation_x.gp {alloy} {seed} {dataName1}")
    os.system(f"gnuplot -c ./src/plot_correlation_y.gp {alloy} {seed} {dataName2}")
    #os.system(f"rm ./src/{dataName1}.txt")
    #os.system(f"rm ./src/{dataName2}.txt")

    ## plot average correlation
    dataName1 = "avgCorXdir"
    dataName2 = "avgCorYdir"
    np.savetxt(f"src/{dataName1}.txt", avgCorXdir.T) # you have to transpose your data since gnuplot read columns
    np.savetxt(f"src/{dataName2}.txt", avgCorYdir.T)
    os.system(f"gnuplot -c ./src/plot_AVGcorrelation.gp {alloy} {seed} {dataName1} {dataName2}")
    #os.system(f"rm ./src/{dataName1}.txt")
    #os.system(f"rm ./src/{dataName2}.txt")

    return 0;

if __name__ == "__main__":
    main()
